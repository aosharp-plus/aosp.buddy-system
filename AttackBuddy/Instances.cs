﻿using AOSharp.Core;
using System.Collections.Generic;
using System.Linq;

namespace AttackBuddy
{
    internal class Instances
    {
        public static List<SimpleChar> _mob = new List<SimpleChar>();
        public static List<SimpleChar> _bossMob = new List<SimpleChar>();
        public static List<SimpleChar> _switchMob = new List<SimpleChar>();

        public void Scanning()
        {
            switch (Playfield.ModelIdentity.Instance)
            {
                case 6123:
                    ScanningInstance6123();
                    break;

                case 1934:
                    ScanningInstance1934();
                    break;

                case 6015:
                    ScanningInstance6015();
                    break;

                case 8020:
                case 8050:
                    ScanningInstance8020();
                    break;

                case 9070:
                    ScanningInstance9070();
                    break;

                case 9061:
                    ScanningInstance9061();
                    break;

                case 4329:
                case 4330:
                case 4331:
                case 4389:
                case 4328:
                case 4391:
                    ScanningInstance4389();
                    break;

                case 1931:
                    ScanningInstance1931();
                    break;

                case 1941:
                    ScanningInstance1941();
                    break;
                case 4365:
                    ScanningInstance4365();
                    break;
                case 4366:
                    ScanningInstance4366();
                    break;
                case 4367:
                    ScanningInstance4367();
                    break;
                default:
                    ScanningDefault();
                    break;
            }
        }

        private void ScanningInstance6123() //s10?
        {
            _switchMob = DynelManager.NPCs
                        .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                            && !Ignores._ignores.Contains(c.Name)
                            && c.Name != "Alien Cocoon" && c.Name != "Alien Coccoon"
                            && c.Name != "Zix"
                            && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                            && c.Name != "Kyr'Ozch Maid"
                            && c.Name != "Kyr'Ozch Technician" && c.Name != "Defense Drone Tower"
                            && c.Name != "Control Leech"
                            && c.Name != "Alien Precision Tower" && c.Name != "Alien Charging Tower"
                            && c.Name != "Alien Shield Tower" && c.Name != "Kyr'Ozch Technician"
                            && c.MaxHealth < 1000000 && IsFightingAny(c))
                        .OrderBy(c => c.Name == "Kyr'Ozch Nurse").ThenBy(c => c.Name == "Kyr'Ozch Offspring")
                        .ThenBy(c => c.Name == "Rimah Corsuezo")
                        .ToList();

            _bossMob = DynelManager.NPCs.Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
            && ((BossHasCorrespondingBuff(287525) && c.Name == "Alien Precision Tower") ||
            (BossHasCorrespondingBuff(287515) && c.Name == "Alien Charging Tower") ||
            (BossHasCorrespondingBuff(287526) && c.Name == "Alien Shield Tower"))).ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                    && !Ignores._ignores.Contains(c.Name)
                    && c.Name != "Zix" && !c.Name.Contains("sapling") && c.Health > 0
                    && c.IsInLineOfSight && c.MaxHealth < 1000000 && IsFightingAny(c)
                    && (!c.IsPet || c.Name == "Drop Trooper - Ilari'Ra"))
                .ToList();
        }

        private void ScanningInstance6015() //12m
        {
            List<string> ignores = new List<string>() { "Left Hand of Insanity", "Strange Mist", "Awakened Xan" };

            _bossMob = DynelManager.NPCs.Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
             && !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
             && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607) && c.MaxHealth >= 1000000)
              .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
               .OrderBy(c => c.Name == "Right Hand of Madness").ToList();

            _switchMob = DynelManager.NPCs
              .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                  && !ignores.Contains(c.Name)
                  && c.Health > 0 && c.IsInLineOfSight && c.MaxHealth < 1000000 && !c.IsPet)
              .OrderBy(c => c.Name == "Green Tower").ThenBy(c => c.Name == "Blue Tower").ToList();
        }

        private void ScanningInstance9070()//Subway Raid
        {
            List<string> ignores = new List<string>() { "Harbinger of Pestilence", "Curse Rot", "Scalding Flames", "Searing Flames",
                "Vergil Doppelganger", "Oblivion", "Ire of Gilgamesh" };

            _bossMob = DynelManager.NPCs.Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                       && !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                       && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607) && c.MaxHealth >= 1000000)
                          .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position)).ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                   && !(ignores.Contains(c.Name) || c.FightingTarget?.Identity == AttackBuddy.Leader)
                   && c.Health > 0 && c.IsInLineOfSight && c.MaxHealth < 1000000 && !c.IsPet)
               .OrderBy(c => c.Name == "Curse-Rotted Grove Sage").ThenBy(c => c.Name.Contains("Director "))
               .ThenBy(c => c.Name == "Lost Thought") .ThenBy(c => c.Name == "Stim Fiend").ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                    && !ignores.Contains(c.Name)
                    && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                    && c.MaxHealth < 1000000)
                .OrderBy(c => c.Name == "Curse-Rotted Grove Sage").ThenBy(c => c.Name.Contains("Director "))
               .ThenBy(c => c.Name == "Lost Thought").ThenBy(c => c.Name == "Stim Fiend").ToList();
        }

        private void ScanningInstance9061()//TOTW Raid
        {
            _bossMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                   && !Ignores._ignores.Contains(c.Name)
                   && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                   && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
                   && c.MaxHealth >= 1000000)
               .OrderBy(c => c.Name == "Khalum the Weaver of Flesh")
               .ThenBy(c => c.Name == "Uklesh the Beguiling")
               .ThenBy(c => c.Name == "Aztur the Immortal")
               .ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                               && !Ignores._ignores.Contains(c.Name)
                               && c.Health > 0 && c.IsInLineOfSight
                               && !c.IsPet && (c.Name == "Devoted Fanatic"
                               || c.Name == "Hallowed Acolyte"
                               || c.Name == "Fanatic"
                               || c.Name == "Turbulent Windcaller"
                               || c.Name == "Ruinous Reverend"
                               || c.Name == "Eternal Guardian"
                               || c.Name == "Defensive Drone"
                               || c.Name == "Confounding Bloated Carcass"
                               || c.Name == "Engorged Sacrificial Husk"
                               || c.Name == "Uklesh the Beguiling"
                               || c.Name == "Khalum the Weaver of Flesh"))
                           .OrderByDescending(c => c.Name == "Engorged Sacrificial Husk")
                           .ThenByDescending(c => c.Name == "Confounding Bloated Carcass")
                           .ThenByDescending(c => c.Name == "Devoted Fanatic")
                           .ThenByDescending(c => c.Name == "Khalum the Weaver of Flesh")
                           .ThenByDescending(c => c.Name == "Uklesh the Beguiling")
                           .ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                                && !Ignores._ignores.Contains(c.Name) && c.Health > 0
                                && c.IsInLineOfSight && c.MaxHealth < 1000000 && !c.IsPet)
                            .OrderByDescending(c => c.Name == "Faithful Cultist")
                            .ThenByDescending(c => c.Name == "Ruinous Reverend")
                            .ThenByDescending(c => c.Name == "Hallowed Acolyte")
                            .ThenByDescending(c => c.Name == "Turbulent Windcaller")
                            .ThenByDescending(c => c.Name == "Seraphic Exarch")
                            .ThenByDescending(c => c.Name == "Cultist Silencer")
                            .ThenByDescending(c => c.Name == "Devoted Fanatic")
                            .ToList();
        }

        private void ScanningInstance1934()//Inner Sanctum
        {
            _bossMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                   && !Ignores._ignores.Contains(c.Name)
                   && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                   && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607))
               .OrderByDescending(c => c.Name == "Hezak the Immortal")
               .OrderByDescending(c => c.Name == "Inobak the Gelid")
               .OrderByDescending(c => c.Name == "Dominus Jiannu")
               .OrderByDescending(c => c.Name == "Dominus Facut the Bloodless")
               .OrderByDescending(c => c.Name == "Dominus Ummoh the Pedagogue")
               .OrderByDescending(c => c.Name == "Jeuru the Defiler")
               .OrderByDescending(c => c.Name == "Iskop the Idolator")
               .ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                               && !Ignores._ignores.Contains(c.Name)
                               && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                               && (c.Name == "Devoted Fanatic"
                               || c.Name == "Hallowed Acolyte"
                               || c.Name == "Fanatic"
                               || c.Name == "Turbulent Windcaller"
                               || c.Name == "Ruinous Reverend"
                               || c.Name == "Eternal Guardian"
                               || c.Name == "Defensive Drone"
                               || c.Name == "Confounding Bloated Carcass"))
                           .OrderByDescending(c => c.Name == "Hallowed Acolyte")
                           .OrderByDescending(c => c.Name == "Confounding Bloated Carcass")
                           .OrderByDescending(c => c.Name == "Devoted Fanatic")
                           .ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                                && !Ignores._ignores.Contains(c.Name) && c.Health > 0
                                && c.IsInLineOfSight && !c.IsPet)
                            .OrderByDescending(c => c.Name == "Faithful Cultist")
                            .OrderByDescending(c => c.Name == "Ruinous Reverend")
                            .OrderByDescending(c => c.Name == "Hallowed Acolyte")
                            .OrderByDescending(c => c.Name == "Turbulent Windcaller")
                            .OrderByDescending(c => c.Name == "Seraphic Exarch")
                            .OrderByDescending(c => c.Name == "Cultist Silencer")
                            .OrderByDescending(c => c.Name == "Devoted Fanatic")
                            .ToList();
        }

        private void ScanningInstance1931()//TOTW
        {
            
            _bossMob = DynelManager.Characters
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                   && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                   && (c.Name == "Aztur the Immortal"
                               || c.Name == "Khalum"
                               || c.Name == "Uklesh the Frozen"
                               || c.Name == "Gartua the Doorkeeper"
                               || c.Name == "Guardian of Tomorrow"
                               || c.Name == "Lien the Memorystalker"
                               || c.Name == "The Re-Animator"
                               || c.Name == "Nematet the Custodian of Time"
                               || c.Name == "The Curator"
                               || c.Name == "Defender of the Three"))
               .ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                               && !Ignores._ignores.Contains(c.Name)
                               && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                               && IsFightingAny(c))
                           .OrderByDescending(c => c.Name == "Cultist")
                           .ThenByDescending(c => c.Name == "Faithful")
                           .ThenByDescending(c => c.Name == "Acolyte")
                           .ThenByDescending(c => c.Name == "Reverend")
                           .ThenByDescending(c => c.Name == "Exarch")
                           .ThenByDescending(c => c.Name == "Deathless Legionnaire")
                           .ThenByDescending(c => c.Name == "Eternal Guardian")
                           .ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                                && !Ignores._ignores.Contains(c.Name) && c.Health > 0
                                && c.IsInLineOfSight && IsFightingAny(c)
                                && !c.IsPet)
                            .ToList();
        }
        private void ScanningInstance4389()//IPande/Pande
        {
            List<string> ignores = new List<string>
            {
              "Amesha Vizaresh",
            };

            _bossMob = DynelManager.NPCs
                       .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                           && !ignores.Contains(c.Name)
                           && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                           && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
                           && c.MaxHealth >= 450000)
                       .ToList();

            _switchMob = DynelManager.NPCs.Where(c => c.Name.Contains("Corrupted Xan-Len") && c.Health > 0 && c.IsInLineOfSight
            && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange).ToList();

            _mob = DynelManager.NPCs
                .Where(c => !ignores.Contains(c.Name) && c.Health > 0
                && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                    && c.IsInLineOfSight && !c.IsPet)
                .OrderByDescending(c => c.Name.Contains("Corrupted Xan-Len"))
                .ThenByDescending(c => c.Name.Contains("Corrupted Xan-Kuir"))
                .ThenByDescending(c => c.Name.Contains("Corrupted Xan-Cur"))
                .ThenByDescending(c => c.Name.Contains("Corrupted Hiisi Berserker"))
                .ThenByDescending(c => c.Name.Contains("Pandemonium Geosurvey Dog")).ToList();
        }
        private void ScanningInstance8020()//POH
        {
            List<string> ignores = new List<string>
            {
              "Flaming Vengeance", "Touched by a Lost Soul", "Lost Soul", "Novictum Manifestation", "Punishment", "Judgment",
                "Altar of Purification", "Altar of Torture", "A Fleeting Memory", "Searing Flames"
            };

            _bossMob = DynelManager.NPCs.Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
            && (c.Name == "The Cybernetic Daemon" || c.Name == "Azdaja the Joyous" || c.Name == "The Maiden" || c.Name.Contains("Phobettor"))
            && c.Health > 0 && c.IsInLineOfSight && !c.IsPet).ToList();

            _switchMob = DynelManager.NPCs.Where(c => (c.Name.Contains("Cultist") || c.Name == "The Sacrifice"
            || c.Name == "Sorrowful Voidling" || c.Name == "Shambling Horror") && !c.IsPet
            && c.Health > 0 && c.IsInLineOfSight && !ignores.Contains(c.Name) && c.MaxHealth < 1000000
            && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange).ToList();

            _mob = DynelManager.NPCs.Where(c => !ignores.Contains(c.Name) && c.Health > 0 && c.MaxHealth < 1000000 && !c.IsPet
            && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange && c.IsInLineOfSight).ToList();
        }

        private void ScanningInstance1941()//Formans
        {
            List<string> ignores = new List<string>
            {
              "Maintenance Worker -   Rodriguez", "Efficiency Expert - Linda Osmos", "Data Analyst - Theresa Cameron", "Blind Eyemutant",
              "Lab Assistant - Evelynn Shead", "Captain \"Rik-Rak\" Jones", "Containment Unit System Droid", "Containment Unit",
            };

            _bossMob = DynelManager.NPCs
                       .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                           && !ignores.Contains(c.Name)
                           && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                           && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
                                       && (c.Name == "Lab Director"))
                       .ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                               && !ignores.Contains(c.Name)
                               && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                               && IsFightingAny(c))
                           .OrderByDescending(c => c.Name == "Executive Defender")
                           .ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                                && !ignores.Contains(c.Name) && c.Health > 0
                                && c.IsInLineOfSight && IsFightingAny(c)
                                && !c.IsPet)
                            .ToList();

        }
        private void ScanningInstance4365()//Sector 13
        {
            List<string> ignores = new List<string>
            {
              "Kyr'Ozch Mine", "Unicorn Recon Agent Chittick", "Biological Transceiver", "Alien Laser Fence",
              "Zix",
            };

            _bossMob = DynelManager.NPCs
                       .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                           && !ignores.Contains(c.Name)
                           && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                           && c.MaxHealth >= 450000)
                       .ToList();

            _switchMob = DynelManager.NPCs.Where(c => (c.Name.Contains("Support Sentry") || c.Name == "Security Patrol - Ankari'Sinuh"
            || c.Buffs.Contains(256509) || c.Buffs.Contains(252409) || c.Buffs.Contains(252411)
            ) && c.Health > 0 && c.IsInLineOfSight && !ignores.Contains(c.Name) && !c.Name.Contains("Unicorn")
            && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange)
                .OrderBy(c => c.Name.Contains("Support Sentry")).ThenBy(c => c.Name == "Security Patrol - Ankari'Sinuh").ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.Name.Contains("Unicorn") && //(!c.Buffs.Contains(256507) || c.GetStat(Stat.ReflectProjectileAC) <= 51) &&
                !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange).ToList();
        }
        private void ScanningInstance4366()//Sector 28
        {
            List<string> ignores = new List<string>
            {
              "Rookie Alien Hunter", "Collision", "Unicorn Laser Fence",
            };

            _bossMob = DynelManager.NPCs
             .Where(c => c.Name == "Embalmer - Cha'Khaz"
             && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
             && !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet && c.MaxHealth >= 4000000).ToList();

            _switchMob = DynelManager.NPCs
            .Where(c => (c.Name.Contains("Alien Cocoon") || c.Name.Contains("Support") ||
                  c.Name.Contains("Drone") || c.FightingTarget?.Name == "Rookie Alien Hunter") &&
                 c.Health > 0 && c.IsInLineOfSight &&
                 !c.IsPet && !ignores.Contains(c.Name) &&
                 DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange)
             .OrderBy(c => c.Name.Contains("Alien Cocoon"))
             .ThenBy(c => c.Name.Contains("Support"))
             .ThenBy(c => c.Name.Contains("Drone"))
             .ToList();

            _mob = DynelManager.NPCs
             .Where(c => !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
             && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange).ToList();
        }
        private void ScanningInstance4367()//Sector 35
        {
            List<string> ignores = new List<string>
            {
              "Kyr'Ozch Mine", "Unicorn Service Tower Alpha", "Unicorn Service Tower Delta", "Unicorn Service Tower Gamma",
              "Unicorn Field Engineer", "Collision spawn tower", "Unicorn Energy Accumulator"
            };

            List<string> assist = new List<string>()
            {
                "Unicorn Service Tower Alpha", "Unicorn Service Tower Delta", "Unicorn Service Tower Gamma"
            };

            _bossMob = DynelManager.NPCs
                       .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                           && !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet && c.MaxHealth >= 450000 &&
                           !c.Buffs.Contains(256507))
                            .OrderBy(c => c.Name == "Field Support  - Cha'Khaz").ToList();

            _switchMob = DynelManager.NPCs.Where(c => (c.Name.Contains("Support") || assist.Contains(c.FightingTarget?.Name))
                  && c.MaxHealth < 450000 && c.Health > 0 && c.IsInLineOfSight && !ignores.Contains(c.Name)
                          && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange).ToList();

            _mob = DynelManager.NPCs
                .Where(c => !ignores.Contains(c.Name) && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange).ToList();
        }

        private void ScanningDefault()
        {
            _bossMob = DynelManager.NPCs
                       .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                           && !Ignores._ignores.Contains(c.Name)
                           && c.Health > 0 && c.IsInLineOfSight && !c.IsPet
                           && !c.Buffs.Contains(253953) && !c.Buffs.Contains(205607)
                           && c.MaxHealth >= 1000000).ToList();

            _switchMob = DynelManager.NPCs
               .Where(c => DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                   && !Ignores._ignores.Contains(c.Name)
                   && c.Name != "Zix" && !c.Name.Contains("sapling")
                   && c.Health > 0 && c.IsInLineOfSight && c.MaxHealth < 1000000 && !c.IsPet
                   && IsFightingAny(c) && (c.Name == "Hand of the Colonel"
                   || c.Name == "Security Supervisor - Ankari'Sinuh"
                  || c.Name == "Hacker'Uri"
                  || c.Name == "Drone Harvester - Jaax'Sinuh"
                  || c.Name == "Support Sentry - Ilari'Uri"
                  || c.Name == "Fanatic"
                  || c.Name == "Alien Coccoon"
                  || c.Name == "Alien Cocoon"
                  || c.Name == "Stasis Containment Field"))
               .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
               .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
               .OrderByDescending(c => c.Name == "Alien Cocoon")
               .OrderByDescending(c => c.Name == "Alien Coccoon" && c.MaxHealth < 40001)
               .ToList();

            _mob = DynelManager.NPCs
                .Where(c => !c.IsPlayer && DynelManager.LocalPlayer.Position.DistanceFrom(c.Position) <= AttackBuddy.SetRange
                    && !Ignores._ignores.Contains(c.Name)
                    && c.Name != "Zix" && !c.Name.Contains("sapling") && c.Health > 0
                    && c.IsInLineOfSight && c.MaxHealth < 1000000 && IsFightingAny(c)
                    && (!c.IsPet || c.Name == "Drop Trooper - Ilari'Ra"))
                .OrderByDescending(c => c.Name == "Drone Harvester - Jaax'Sinuh")
                .OrderByDescending(c => c.Name == "Support Sentry - Ilari'Uri")
                .OrderByDescending(c => c.Name == "Alien Cocoon")
                .OrderByDescending(c => c.Name == "Alien Coccoon" && c.MaxHealth < 40001)
                .OrderByDescending(c => c.Name == "Masked Operator")
                .OrderByDescending(c => c.Name == "Masked Technician")
                .OrderByDescending(c => c.Name == "Masked Engineer")
                .OrderByDescending(c => c.Name == "Masked Superior Commando")
                .OrderByDescending(c => c.Name == "Hacker'Uri")
                .OrderByDescending(c => c.Name == "Hand of the Colonel")
                .ToList();
        }

        bool BossHasCorrespondingBuff(int buff)
        {
            return DynelManager.NPCs
                .Where(c => c.Name == "Kyr'Ozch Technician" && c.Buffs.Contains(buff))
                .Any();
        }

        bool IsFightingAny(SimpleChar mob)
        {
            if (mob?.FightingTarget == null) { return true; }

            if (mob?.FightingTarget?.Name == "Guardian Spirit of Purification") { return true; }

            if (Team.IsInTeam)
            {
                return Team.Members.Select(m => m.Name).Contains(mob.FightingTarget?.Name)
                   || AttackBuddy._helpers.Contains(mob?.FightingTarget.Name)
                   || (mob?.FightingTarget?.IsPet == true
                        && Team.Members.Select(c => c.Identity.Instance).Any(c => c == mob?.FightingTarget?.PetOwnerId));
            }

            return mob.FightingTarget?.Name == DynelManager.LocalPlayer.Name
                || AttackBuddy._helpers.Contains(mob?.FightingTarget.Name)
                || (mob?.FightingTarget?.IsPet == true
                    && mob.FightingTarget?.PetOwnerId == DynelManager.LocalPlayer.Identity.Instance);
        }
    }
}
