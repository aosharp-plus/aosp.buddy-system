﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System.Linq;
using Shared;
using AOSharp.Common.Unmanaged.Imports;
using System;

namespace AttackBuddy
{
    internal class HandleAttacking
    {
        public static int AttackRange;
        public static int TauntRange;

        AttackState State;

        public void AttackBase()
        {
            var localPlayer = DynelManager.LocalPlayer;

            var leader = DynelManager.Characters.FirstOrDefault(c => c.Identity == AttackBuddy.Leader && c.IsValid
            && c.DistanceFrom(localPlayer) < AttackRange && c.IsInLineOfSight
            && !c.Buffs.Contains(280470) && !c.Buffs.Contains(257127) && !c.Buffs.Contains(260301) && !c.Buffs.Contains(NanoLine.Stun)
            && !c.Buffs.Contains(NanoLine.Fear) && !c.Buffs.Contains(291135) && !c.Buffs.Contains(NanoLine.MindControl)
            && !c.Buffs.Contains(280813) && !c.Buffs.Contains(302676));

            var boss = Instances._bossMob.
                Where(c => c.Health > 0).OrderBy(o => o.Position.DistanceFrom(localPlayer.Position)).FirstOrDefault();

            var switchMob = Instances._switchMob.
                Where(c => c.Health > 0)
                .OrderBy(o => o.Position.DistanceFrom(localPlayer.Position)).FirstOrDefault();

            var mob = Instances._mob.Where(c => c.Health > 0 && c.Identity != boss?.Identity)
                .OrderBy(h => h.Position.DistanceFrom(localPlayer.Position)).FirstOrDefault(c => c.Position.DistanceFrom(localPlayer.Position) <= AttackRange);

            SimpleChar attackingMob = null;

            if (AttackBuddy._settings["Switching"].AsBool())
            {
                attackingMob = DynelManager.NPCs.Where(a => a.Health > 0 && !a.Buffs.Contains(256507) && a.Identity != boss?.Identity
                && (a.Identity == mob?.Identity || a.Identity == switchMob?.Identity) && a.IsAttacking && !a.IsPet
                 && Team.Members.Any(t => t.Identity == a.FightingTarget?.Identity) && a.FightingTarget?.Identity != AttackBuddy.Leader)
                    .OrderBy(o => o.Position.DistanceFrom(localPlayer.Position)).FirstOrDefault();
            }

            if (localPlayer.Identity == AttackBuddy.Leader || leader == null)
            {
                if (switchMob != null)
                {
                    if (ValidTarget(switchMob))
                    {
                        HandleAttack(switchMob);
                    }
                }
                else if (attackingMob != null)
                {
                    if (ValidTarget(attackingMob))
                    {
                        HandleAttack(attackingMob);
                    }
                }
                else if (mob != null)
                {
                    if (ValidTarget(mob))
                    {
                        HandleAttack(mob);
                    }
                }
                else if (boss != null)
                {
                    if (ValidTarget(boss))
                    {
                        HandleAttack(boss);
                    }
                }
                else if (AttackBuddy._settings["Taunt"].AsBool())
                {
                    var outOfRangeMob = Instances._mob.OrderBy(o => o.Position.DistanceFrom(localPlayer.Position)).FirstOrDefault();
                    if (outOfRangeMob == null) { return; }

                    if (ShouldTaunt(outOfRangeMob))
                    {
                        TauntingTools.HandleTaunting(outOfRangeMob);
                    }
                }
                else if (localPlayer.IsAttacking)
                {
                    if (State == AttackState.Stop) { return; }
                    StopAttacking();
                    State = AttackState.Stop;
                }
            }
            else
            {
                if (leader?.IsAttacking == true)
                {
                    HandleAttack(leader?.FightingTarget);
                }
            }
        }
        enum AttackState { Waiting, Atack, Stop }
        bool ValidTarget(SimpleChar target)
        {
            return !target.Buffs.Contains(253953) &&
                    !target.Buffs.Contains(NanoLine.ShovelBuffs) &&
                    !target.IsPlayer && !target.IsPet && target.IsInLineOfSight &&
                    !target.Buffs.Contains(NanoLine.CharmOther) &&
                    !target.Buffs.Contains(NanoLine.Charm_Short);
        }
        bool ShouldTaunt(SimpleChar target)
        {
            return target?.IsInLineOfSight == true
                && !DynelManager.LocalPlayer.IsMoving
                && !DynelManager.LocalPlayer.IsAttacking
                && !DynelManager.LocalPlayer.IsAttackPending
                && target?.MaxHealth < 450000
                && target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) <= TauntRange
                && target?.Position.DistanceFrom(DynelManager.LocalPlayer.Position) > AttackRange;
        }
        void HandleAttack(SimpleChar target)
        {
            var localPlayer = DynelManager.LocalPlayer;
            if (target == null) { StopAttacking(); return; }
            var inRange = target?.Position.DistanceFrom(localPlayer.Position) <= AttackRange;

            if (localPlayer.IsAttacking == true)
            {
                if (ShouldStopAttack(target) || localPlayer.FightingTarget?.Identity != target?.Identity)
                {
                    if (State == AttackState.Stop) { return; }
                    StopAttacking();
                    State = AttackState.Stop;
                }
            }
            else if (inRange && target.IsInLineOfSight)
            {
                if (localPlayer.FightingTarget == null && localPlayer.IsAttackPending == false)
                {
                    localPlayer.Attack(target, false);
                    State = AttackState.Atack;
                }
            }
        }

        bool ShouldStopAttack(SimpleChar target)
        {
            var localPlayerBuffs = DynelManager.LocalPlayer?.Buffs;

            if (target == null) { return false; }

            //Name: Khalum, Id int: 1786491342, Identity: (SimpleChar: 6A7BB1CE), type: SimpleChar
            //205633 Other 205633
            //205607 Immortal 205607
            //205611 Soul of Khalum Pulser 205611

            return target.Buffs.Contains(253953) || //target.Buffs.Contains(205607) ||
                   localPlayerBuffs.Contains(305862) || localPlayerBuffs.Contains(281114) ||
                   DynelManager.LocalPlayer.Position.Distance2DFrom(target.Position) > AttackRange ||
                   !target.IsInLineOfSight || target.IsPet;
        }

        public void StopAttacking()
        {
            IntPtr instance = N3Engine_t.GetInstance();
            if (!(instance == IntPtr.Zero))
            {
                N3EngineClientAnarchy_t.StopAttack(instance);
            }
        }
    }
}
