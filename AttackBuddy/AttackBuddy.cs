﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Common.Unmanaged.Imports;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using AttackBuddy.IPCMessages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace AttackBuddy
{
    public class AttackBuddy : AOPluginEntry
    {
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static int SetRange;

        public static bool Enable = false;
        public static bool _init = false;

        public static Identity Leader = Identity.None;

        public static double _stateTimeOut = Time.NormalTime;

        double LeaderTimeout;
        double BroadcastTime;

        private static Window _infoWindow;
        private static Window _helperWindow;

        private static View _helperView;

        public static Settings _settings;

        public static List<string> _helpers = new List<string>();

        public static string previousErrorMessage = string.Empty;


        public override void Run()
        {
            try
            {
                _settings = new Settings("AttackBuddy");

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\AOSharp\\AttackBuddy\\{DynelManager.LocalPlayer.Name}\\Config.json");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.StartStop, OnStartStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.RangeInfo, OnRangeInfoMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.LeaderInfo, OnLeaderInfoMessage);

                Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannelChangedEvent += IPCChannel_Changed;
                Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRangeChangedEvent += AttackRange_Changed;
                Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRangeChangedEvent += TauntRange_Changed;

                Chat.RegisterCommand("enable", BuddyCommand);

                Chat.RegisterCommand("info", (string command, string[] param, ChatWindow chatWindow) =>
                {
                    if (Leader != Identity.None)
                    {
                        var leader = DynelManager.Players.FirstOrDefault(p => p.Identity == Leader);
                        if (leader != null)
                        {
                            Chat.WriteLine($"Leader is {leader.Name}");
                        }
                    }

                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        foreach (var mob in Instances._mob)
                        {
                            Chat.WriteLine($"mob: {mob.Name}");
                        }

                        foreach (var boss in Instances._bossMob)
                        {
                            Chat.WriteLine($"Boss: {boss.Name}");
                        }

                        foreach (var switchMob in Instances._switchMob)
                        {
                            Chat.WriteLine($"Switch mob: {switchMob.Name}");
                        }
                    }
                });

                SettingsController.RegisterSettingsWindow("AttackBuddy", PluginDirectory + "\\UI\\AttackBuddySettingWindow.xml", _settings);

                Game.OnUpdate += OnUpdate;

                _settings.AddVariable("TeamLead", false);
                _settings.AddVariable("Switching", false);
                _settings.AddVariable("Rings", false);

                _settings.AddVariable("Enable", false);
                _settings["Enable"] = false;

                _settings.AddVariable("Taunt", false);

                if (!Game.IsNewEngine)
                {
                    Chat.WriteLine("AttackBuddy Loaded!");
                    Chat.WriteLine("/buddy for settings. /enable to start and stop.");
                }
                else
                {
                    Chat.WriteLine("Does not work on this engine!");
                }

                HandleAttacking.AttackRange = Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange;
                HandleAttacking.TauntRange = Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange;
                SetRange = Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange;
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public Window[] _windows => new Window[] { _helperWindow };

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }
        public static void AttackRange_Changed(object s, int e)
        {
            Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange = e;
            Config.Save();
        }
        public static void TauntRange_Changed(object s, int e)
        {
            Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange = e;
            Config.Save();
        }

        private void Start()
        {
            Enable = true;

            Chat.WriteLine("AttackBuddy enabled.");
        }

        private void Stop()
        {
            Enable = false;

            Chat.WriteLine("AttackBuddy disabled.");
        }

        private void OnStartStopMessage(int sender, IPCMessage msg)
        {
            if (msg is StartStopIPCMessage startStopMessage)
            {
                if (startStopMessage.IsStarting)
                {
                    _settings["Enable"] = true;
                    Start();
                }
                else
                {
                    _settings["Enable"] = false;
                    Stop();
                }
            }
        }

        private void OnRangeInfoMessage(int sender, IPCMessage msg)
        {
            if (msg is RangeInfoIPCMessage rangeInfoMessage)
            {
                Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange = rangeInfoMessage.AttackRange;
                Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange = rangeInfoMessage.TauntRange;
                SetRange = rangeInfoMessage.TauntRange;
            }
        }

        private void OnLeaderInfoMessage(int sender, IPCMessage msg)
        {
            if (msg is LeaderInfoIPCMessage leaderInfoMessage)
            {
                //Chat.WriteLine($"Received request: {leaderInfoMessage.Request}");

                switch (leaderInfoMessage.Request)
                {
                    case 0: // no
                        var leader = DynelManager.Players.FirstOrDefault(l => l.Identity == leaderInfoMessage.LeaderIdentity);
                        //Chat.WriteLine($"New leader received, leader = {leader.Name}");
                        Leader = leaderInfoMessage.LeaderIdentity;
                        break;
                    case 1: // yes
                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            //Chat.WriteLine("Received leader request.");
                            IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader, Request = 0 });
                        }
                        break;
                    case 2: // reset leader.
                            //Chat.WriteLine("Resetting leader");
                        Leader = Identity.None;

                        break;
                }
            }
        }

        #region buttons

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDirectory + "\\UI\\AttackBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void HandleHelpersViewClick(object s, ButtonBase button)
        {
            var window = _windows.Where(c => c != null && c.IsValid).FirstOrDefault();

            if (window != null)
            {
                if (window.Views.Contains(_helperView)) { return; }

                _helperView = View.CreateFromXml(PluginDirectory + "\\UI\\AttackBuddyHelpersView.xml");
                SettingsController.AppendSettingsTab(window, new WindowOptions() { Name = "Helpers", XmlViewName = "AttackBuddyHelpersView" }, _helperView);
            }
            else if (_helperWindow == null || (_helperWindow != null && !_helperWindow.IsValid))
            {
                SettingsController.CreateSettingsTab(_helperWindow, PluginDirectory, new WindowOptions() { Name = "Helpers", XmlViewName = "AttackBuddyHelpersView" }, _helperView, out var container);
                _helperWindow = container;
            }
        }

        private void HandleAddHelperViewClick(object s, ButtonBase button)
        {
            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                window.FindView("HelperNameBox", out TextInputView nameInput);

                if (nameInput != null && !string.IsNullOrEmpty(nameInput.Text))
                {
                    _helpers.Add(nameInput.Text);
                }
            }
        }
        private void HandleRemoveHelperViewClick(object s, ButtonBase button)
        {
            var window = SettingsController.FindValidWindow(_windows);

            if (window != null && window.IsValid)
            {
                window.FindView("HelperNameBox", out TextInputView nameInput);

                if (nameInput != null && !string.IsNullOrEmpty(nameInput.Text))
                {
                    _helpers.Remove(nameInput.Text);
                }
            }
        }

        private void HandleClearHelpersViewClick(object s, ButtonBase button)
        {
            _helpers.Clear();
        }

        private void HandlePrintHelpersViewClick(object s, ButtonBase button)
        {
            foreach (string str in _helpers)
            {
                Chat.WriteLine(str);
            }
        }

        #endregion

        private void OnUpdate(object s, float deltaTime)
        {
            try
            {
                var localPlayer = DynelManager.LocalPlayer;

                if (Game.IsZoning)
                {
                    Enable = false;
                    _settings["Enable"] = false;
                    Leader = Identity.None;
                    return;
                }

                if (_settings["Rings"].AsBool())
                {
                    Debug_DrawCircle(localPlayer.Position, Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange, DebuggingColor.Red);
                    Debug_DrawCircle(localPlayer.Position, Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange, DebuggingColor.Green);
                }

                if (Team.IsInTeam)
                {
                    if (!_settings["TeamLead"].AsBool())
                    {
                        var leaderMember = Team.Members.FirstOrDefault(m => m.IsLeader);

                        if (Leader == Identity.None)
                        {
                            if (Time.AONormalTime > BroadcastTime)
                            {
                                //Chat.WriteLine("Leader is none. Requesting leader from IPC.");
                                IPCChannel.Broadcast(new LeaderInfoIPCMessage() { Request = 1 });
                                LeaderTimeout = Time.AONormalTime + 0.5;
                                BroadcastTime = Time.AONormalTime + 1.0;
                            }

                            if (Time.AONormalTime > LeaderTimeout)
                            {
                                if (leaderMember != null)
                                {
                                    //Chat.WriteLine($" Leader = Team Lead, {leaderMember.Name}.");
                                    Leader = leaderMember.Identity;
                                }
                            }
                        }
                        else
                        {
                            if (localPlayer.Identity == Leader)
                            {
                                if (localPlayer.Identity != leaderMember?.Identity)
                                {
                                    IPCChannel.Broadcast(new LeaderInfoIPCMessage() { Request = 2 });
                                    //Chat.WriteLine("Not Leader anymore :( ");
                                    Leader = Identity.None;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Leader != localPlayer.Identity)
                        {
                            //Chat.WriteLine($"Team Lead checked, leader = {localPlayer.Name}.");
                            IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = localPlayer.Identity, Request = 0 });
                            Leader = localPlayer.Identity;
                        }
                    }
                }
                else
                {
                    if (Leader == Identity.None)
                    {
                        //Chat.WriteLine($"Solo, Leader = {localPlayer.Name}.");
                        Leader = localPlayer.Identity;
                    }
                }

                var instances = new Instances();
                var attacking = new HandleAttacking();

                #region UI


                var window = SettingsController.FindValidWindow(_windows);

                if (window != null && window.IsValid)
                {
                    if (window.FindView("AttackBuddyAddHelper", out Button addHelperView))
                    {
                        addHelperView.Tag = window;
                        addHelperView.Clicked = HandleAddHelperViewClick;
                    }

                    if (window.FindView("AttackBuddyRemoveHelper", out Button removeHelperView))
                    {
                        removeHelperView.Tag = window;
                        removeHelperView.Clicked = HandleRemoveHelperViewClick;
                    }

                    if (window.FindView("AttackBuddyClearHelpers", out Button clearHelpersView))
                    {
                        clearHelpersView.Tag = window;
                        clearHelpersView.Clicked = HandleClearHelpersViewClick;
                    }

                    if (window.FindView("AttackBuddyPrintHelpers", out Button printHelpersView))
                    {
                        printHelpersView.Tag = window;
                        printHelpersView.Clicked = HandlePrintHelpersViewClick;
                    }
                }

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);
                    SettingsController.settingsWindow.FindView("AttackRangeBox", out TextInputView attackRangeInput);
                    SettingsController.settingsWindow.FindView("TauntRangeBox", out TextInputView TauntRangeInput);

                    if (channelInput != null)
                    {
                        if (int.TryParse(channelInput.Text, out int channelValue)
                            && Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel != channelValue)
                        {
                            Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel = channelValue;
                        }
                    }

                    bool attackRangeChanged = false;

                    if (int.TryParse(attackRangeInput.Text, out int attackRangeInputValue) && Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange != attackRangeInputValue)
                    {
                        Chat.WriteLine($"attackRange changed = {attackRangeInputValue}");
                        HandleAttacking.AttackRange = attackRangeInputValue;
                        Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange = attackRangeInputValue;
                        attackRangeChanged = true;
                    }


                    if (int.TryParse(TauntRangeInput.Text, out int TauntRangeInputValue))
                    {
                        if (_settings["Taunt"].AsBool())
                        {
                            if (Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange != TauntRangeInputValue)
                            {
                                HandleAttacking.TauntRange = TauntRangeInputValue;
                                SetRange = TauntRangeInputValue;
                                Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange = TauntRangeInputValue;
                            }
                        }
                        else
                        {
                            var attackRangePlusOne = Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange + 1;
                            if (attackRangePlusOne != TauntRangeInputValue)
                            {
                                TauntRangeInput.Text = attackRangePlusOne.ToString();
                                SetRange = Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange + 1;
                            }
                        }
                    }

                    if (attackRangeChanged)
                    {
                        IPCChannel.Broadcast(new RangeInfoIPCMessage()
                        {
                            AttackRange = Config.CharSettings[DynelManager.LocalPlayer.Name].AttackRange,
                            TauntRange = Config.CharSettings[DynelManager.LocalPlayer.Name].TauntRange
                        });
                    }

                    if (SettingsController.settingsWindow.FindView("AttackBuddyInfoView", out Button infoView))
                    {
                        infoView.Tag = SettingsController.settingsWindow;
                        infoView.Clicked = HandleInfoViewClick;
                    }

                    if (SettingsController.settingsWindow.FindView("AttackBuddyHelpersView", out Button helperView))
                    {
                        helperView.Tag = SettingsController.settingsWindow;
                        helperView.Clicked = HandleHelpersViewClick;
                    }

                    if (!_settings["Enable"].AsBool() && Enable)
                    {
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                    if (_settings["Enable"].AsBool() && !Enable)
                    {
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }
                }

                #endregion

                if (!_settings["Enable"].AsBool()) { return; }

                instances.Scanning();
                attacking.AttackBase();

            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public void Debug_DrawCircle(Vector3 position, float radius, Vector3 color, int numVertices = 12)
        {
            numVertices = Math.Max(numVertices, 3);
            var vertices = new Vector3[numVertices];

            for (int i = 0; i < numVertices; i++)
            {
                vertices[i] = position + (Quaternion.AngleAxis((360f / numVertices) * i, Vector3.Up) * Vector3.Forward) * radius;

                if (i > 0)
                {
                    Debug.DrawLine(vertices[i - 1], vertices[i], color);

                    if (i == numVertices - 1)
                    {
                        Debug.DrawLine(vertices[0], vertices[i], color);
                    }
                }
            }
        }

        private void BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Enable"].AsBool())
                    {
                        _settings["Enable"] = true;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }
                    else
                    {
                        _settings["Enable"] = false;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                    return;
                }

                switch (param[0].ToLower())
                {
                    case "ignore":
                        if (param.Length > 1)
                        {
                            string name = string.Join(" ", param.Skip(1));

                            if (!Ignores._ignores.Contains(name))
                            {
                                Ignores._ignores.Add(name);
                                chatWindow.WriteLine($"Added \"{name}\" to ignored mob list");
                            }
                            else if (Ignores._ignores.Contains(name))
                            {
                                Ignores._ignores.Remove(name);
                                chatWindow.WriteLine($"Removed \"{name}\" from ignored mob list");
                            }
                        }
                        else
                        {
                            chatWindow.WriteLine("Please specify a name");
                        }
                        break;

                    default:
                        return;
                }
                Config.Save();
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    chatWindow.WriteLine(errorMessage);
                    chatWindow.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public static int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;

            var lineMatch = Regex.Match(ex.StackTrace ?? "", @":line (\d+)$", RegexOptions.Multiline);

            if (lineMatch.Success)
            {
                lineNumber = int.Parse(lineMatch.Groups[1].Value);
            }

            return lineNumber;
        }
    }
}
