﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;

namespace VortexxBuddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.XanHubId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._startPos) < 10.0f
                && Extensions.CanProceed())
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Died");

            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) > 20.0f)
                {
                    if (DynelManager.LocalPlayer.HealthPercent > 65 && DynelManager.LocalPlayer.NanoPercent > 65)
                    {
                        if (DynelManager.LocalPlayer.MovementState == MovementState.Sit)
                        {
                            MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1)
                            {
                                if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                                {
                                    VortexxBuddy.NavMeshMovementController.SetNavMeshDestination(Constants._startPos);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}