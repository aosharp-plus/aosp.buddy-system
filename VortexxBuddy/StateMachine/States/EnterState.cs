﻿using AOSharp.Core;
using AOSharp.Core.UI;

namespace VortexxBuddy
{
    public class EnterState : IState
    {
        private static double _time;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.VortexxId
                && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPodium) < 5)
            {
                return new FightState();
            }

            if (Playfield.ModelIdentity.Instance == Constants.XanHubId)
            {
                if (!Extensions.CanProceed())
                {
                    return new IdleState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (Extensions.CanProceed())
            {
                Chat.WriteLine("Entering");
                VortexxBuddy.VortexxCorpse = false;
            }

            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (!Team.IsInTeam && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._startPos) > 3)
            {
                if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                {
                    if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                    {
                        VortexxBuddy.NavMeshMovementController.SetNavMeshDestination(Constants._startPos);
                    }
                }
            }

            if (Team.IsInTeam && Time.AONormalTime > _time + 2f)
            {
                _time = Time.AONormalTime;

                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 20)
                {
                    if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                    {
                        VortexxBuddy.NavMeshMovementController.SetDestination(Constants._entrance);
                    }
                }
            }

            if (Playfield.ModelIdentity.Instance == Constants.VortexxId)
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPodium) > 2f)
                {
                    if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                    {
                        if (!VortexxBuddy.NavMeshMovementController.IsNavigating)
                        {
                            VortexxBuddy.NavMeshMovementController.SetNavMeshDestination(Constants._centerPodium);
                        }
                    }
                }
            }
        }
    }
}