﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using System.Linq;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using VortexxBuddy.IPCMessages;
using AOSharp.Core.UI;

namespace VortexxBuddy
{
    public class FightState : IState
    {
        private static SimpleChar _vortexx;
        private static SimpleChar _releasedSpirit;
        private static SimpleChar _desecratedSpirits;
        private static SimpleChar _notumErruption;
        private static Dynel _nanoErruption;
        private static Dynel _cleansingBreeze;

        private static Corpse _vortexxCorpse;
        private static Corpse _releasedSpiritCorpse;

        public static bool _shout = false;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance != Constants.VortexxId)
            {
                return new IdleState();
            }
            else
            {
               GetDynels();

                if (VortexxBuddy._settings["Immunity"].AsBool() && _releasedSpirit != null)
                {
                    if (DynelManager.LocalPlayer.IsAttacking)
                    {
                        DynelManager.LocalPlayer.StopAttack(false);
                    }
                    else
                    {
                        return new ImmunityState();
                    }
                }

                if (VortexxBuddy._settings["Farming"].AsBool())
                {
                    if (_desecratedSpirits == null && VortexxBuddy.VortexxCorpse)
                    {
                        return new FarmingState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Network.ChatMessageReceived += VortexxShout;

            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }

            _shout = false;
            VortexxBuddy._clearToEnter = false;

            Chat.WriteLine("Fight!");
        }

        public void OnStateExit()
        {
            Network.ChatMessageReceived -= VortexxShout;

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack(false);
            }
            if (VortexxBuddy.NavMeshMovementController.IsNavigating)
            {
                VortexxBuddy.NavMeshMovementController.Halt();
            }

            _shout = false;
        }

        public static void VortexxShout(object s, ChatMessageBody msg)
        {
            if (msg.PacketType == ChatMessageType.NpcMessage)
            {
                var npcMsg = (NpcMessage)msg;

                string[] triggerMsg = new string[4] { "Flee you pathetic insects", "Fear my power", "I will have your heads", "Breathe in the terror" };

                if (triggerMsg.Any(x => npcMsg.Text.Contains(x)))
                {
                    _shout = true;
                }
            }
        }

        public void Tick()
        {
            if (!Team.IsInTeam || Game.IsZoning) { return; }

            if (Playfield.ModelIdentity.Instance == Constants.VortexxId)
            {
                GetDynels();

                if (_shout)
                {
                    if (_nanoErruption != null)
                    {
                        if(DynelManager.LocalPlayer.Position.DistanceFrom(_nanoErruption.Position) > 1)
                        {
                            if (!MovementController.Instance.IsNavigating)
                            {
                                VortexxBuddy.NavMeshMovementController.SetNavMeshDestination(_nanoErruption.Position);
                            }
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.Buffs.Contains(VortexxBuddy.Nanos.NanoInfusion))
                            {
                                _shout = false;
                            }
                        }
                    }
                    else
                    {
                        HandlePathtoCenter();
                    }
                }
                else
                {
                    HandlePathtoCenter();

                    if (_vortexx != null)
                    {
                        HandleAttacking(_vortexx);
                    }
                    else
                    {
                        if (_desecratedSpirits != null)
                        {
                            HandleAttacking(_desecratedSpirits);
                        }
                    }
                }
                    
                if (_releasedSpiritCorpse != null)
                {
                    if (DynelManager.LocalPlayer.Identity == VortexxBuddy.Leader)
                    {
                        if (!VortexxBuddy._clearToEnter && _releasedSpiritCorpse.Position.DistanceFrom(Constants._bluePodium) < 5)
                        {
                            VortexxBuddy.IPCChannel.Broadcast(new EnterMessage());
                            VortexxBuddy._clearToEnter = true;
                        }
                    }
                }

                if (_vortexxCorpse != null)
                {
                    VortexxBuddy.VortexxCorpse = true;
                }
            }
        }
        private void HandlePathtoCenter()
        {
            if (!DynelManager.LocalPlayer.Buffs.Contains(VortexxBuddy.Nanos.Terrified)
                        && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPodium) > 2f)
            {
                if (!MovementController.Instance.IsNavigating)
                {
                    VortexxBuddy.NavMeshMovementController.SetNavMeshDestination(Constants._centerPodium);
                }
            }
        }

        private void HandleAttacking(SimpleChar target)
        {
            if (target != null)
            {
                if (DynelManager.LocalPlayer.FightingTarget == null
                        && !DynelManager.LocalPlayer.IsAttackPending)
                {
                    DynelManager.LocalPlayer.Attack(target, false);
                }
            }
        }

        private void GetDynels()
        {
            _vortexx = DynelManager.NPCs
                 .Where(c => c.Health > 0
                  && c.Name == "Ground Chief Vortexx")
                  .FirstOrDefault();

            _desecratedSpirits = DynelManager.NPCs
               .Where(c => c.Health > 0
                       && c.Name == "Desecrated Spirit")
                   .FirstOrDefault();

            _releasedSpirit = DynelManager.NPCs
                .Where(c => c.Health > 0
                       && c.Name == "Released Spirit")
                   .FirstOrDefault();

            _vortexxCorpse = DynelManager.Corpses
              .Where(c => c.Name == "Remains of Ground Chief Vortexx")
                  .FirstOrDefault();

            _releasedSpiritCorpse = DynelManager.Corpses
              .Where(c => c.Name == "Remains of Released Spirit")
                  .FirstOrDefault();

            _notumErruption = DynelManager.NPCs
               .Where(c => c.Name == "Notum Erruption")
                  .FirstOrDefault();

            _nanoErruption = DynelManager.AllDynels
               .Where(c => c.Name == "Nano Eruption")
                  .FirstOrDefault();

            _cleansingBreeze = DynelManager.AllDynels
              .Where(c => c.Name =="Cleansing Breeze")
                 .FirstOrDefault();
        }
    }
}