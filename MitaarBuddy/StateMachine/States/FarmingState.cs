﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class FarmingState : IState
    {
        public static bool _initCorpse = false;
        public static bool _atCorpse = false;
        private static double _timeToLeave;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance != Constants.MitaarId)
            {
                if (DynelManager.LocalPlayer.Identity == MitaarBuddy.Leader)
                {
                    if (!Team.Members.Any(c => c.Character == null) || MitaarBuddy._settings["Solo"].AsBool())
                    {
                        return new ReformState();
                    }
                }
                else
                {
                    return new IdleState();
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            _atCorpse = false;
            MitaarBuddy.SinuhCorpse = false;
            Chat.WriteLine("Looting state");
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Playfield.ModelIdentity.Instance == Constants.MitaarId)
            {
                var Device = DynelManager.AllDynels.FirstOrDefault(c => c.Name == "Strange Alien Device");
                var SinuhCorpse = DynelManager.Corpses.FirstOrDefault(c => c.Name == "Remains of Technomaster Sinuh");

                if (SinuhCorpse != null)
                {
                    if (!_atCorpse)
                    {
                        if (AtPosition(SinuhCorpse.Position, 2))
                        {
                            Chat.WriteLine("Pause for looting, 30 sec");
                            _timeToLeave = Time.AONormalTime + 30;
                            _atCorpse = true;
                        }
                    }
                    else
                    {
                        if (Time.AONormalTime > _timeToLeave)
                        {
                            HandleDeviceUse(Device);
                        }
                    }
                }
                else
                {
                    HandleDeviceUse(Device);
                }
            }
        }

        void HandleDeviceUse(Dynel Device)
        {
            if (Device != null)
            {
                if (Extensions.CanProceed())
                {
                    if (AtPosition(Constants._strangeAlienDevice, 3))
                    {
                        if (DynelManager.LocalPlayer.Identity == MitaarBuddy.Leader)
                        {
                            Device.Use();
                        }
                        else
                        {
                            var _leader = Team.Members
                                  .Where(c => c.Character?.Health > 0
                                      && c.Character?.IsValid == true
                                      && c.Identity == MitaarBuddy.Leader)
                                  .FirstOrDefault()?.Character;

                            if (_leader == null)
                            {
                                Device.Use();
                            }
                        }
                    }
                }
            }
        }

        bool AtPosition(Vector3 position, int distance)
        {
            HandlePathing(position, distance);
            return MovementController.Instance.IsNavigating == false;
        }

        void HandlePathing(Vector3 position, int distance)
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(position) > distance)
            {
                if (!MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.SetDestination(position);
                }
            }
            else
            {
                if (MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.Halt();
                }
            }
        }
    }
}