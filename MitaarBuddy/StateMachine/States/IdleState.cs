﻿using AOSharp.Core;
using System.Linq;

namespace MitaarBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (!MitaarBuddy._settings["Enable"].AsBool()) { return null; }
            else
            {
                if (Playfield.ModelIdentity.Instance == Constants.XanHubId || Playfield.Name == "Central Gateway")
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 20.0f)
                    {
                        if (Team.IsInTeam)
                        {
                            if (Extensions.CanProceed())
                            {
                                if (DynelManager.LocalPlayer.Identity == MitaarBuddy.Leader)
                                {
                                    if (!Team.Members.Any(t => t.Character == null) || MitaarBuddy._settings["Solo"].AsBool())
                                    {
                                        return new EnterState();
                                    }
                                }
                                else
                                {
                                    var _leader = Team.Members
                                    .Where(c => c.Character?.Health > 0
                                        && c.Character?.IsValid == true
                                        && c.Identity == MitaarBuddy.Leader)
                                    .FirstOrDefault()?.Character;

                                    if (_leader == null)
                                    {
                                        return new EnterState();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return new DiedState();
                    }
                }

                if (Playfield.ModelIdentity.Instance == Constants.MitaarId)
                {
                    if (!MitaarBuddy._settings["Solo"].AsBool())
                    {
                        return new FightState();
                    }
                    else
                    {
                        return new SoloState();
                    }  
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
        }
    }
}
