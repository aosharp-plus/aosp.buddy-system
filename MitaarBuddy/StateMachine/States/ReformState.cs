﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace MitaarBuddy
{
    public class ReformState : IState
    {
        ReformPhase _phase;

        public static List<Identity> _teamCache = new List<Identity>();

        List<Identity> _invitedList = new List<Identity>();

        public IState GetNextState()
        {
            if (_phase == ReformPhase.Completed)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Reforming");
            _phase = ReformPhase.Disbanding;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("Done Reforming");
            _invitedList.Clear();
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            switch (_phase)
            {
                case ReformPhase.Disbanding:
                    if (!Team.IsInTeam)
                    {
                        _phase = ReformPhase.Inviting;
                    }
                    else
                    {
                        if (!Team.Members.Any(t => t.Character == null) || MitaarBuddy._settings["Solo"].AsBool())
                        {
                            Team.Disband();
                        }
                    }
                    return;
                case ReformPhase.Inviting:
                    if (Team.IsInTeam)
                    {
                        if (_teamCache.Count == Team.Members.Count)
                        {
                            _phase = ReformPhase.Completed;
                        }
                    }

                    InvitePlayers();
                    return;

            }
        }
        void InvitePlayers()
        {
            foreach (var player in _teamCache.Where(p => !Team.Members.Contains(p) 
            && !_invitedList.Contains(p) && p != DynelManager.LocalPlayer.Identity))
            {
                _invitedList.Add(player);
                Team.Invite(player);
            }
        }
        enum ReformPhase
        {
            Disbanding,
            Inviting,
            Completed,
        }
    }
}

