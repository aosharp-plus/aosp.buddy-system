﻿using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class EnterState : IState
    {
        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.MitaarId)
            {
                if (!MitaarBuddy._settings["Solo"].AsBool())
                {
                    if (!Team.Members.Any(t => t.Character == null))
                    {
                        return new FightState();
                    }
                }
                else
                {
                    return new SoloState();
                }
            }
            return null;
        }

        public void OnStateEnter()
        {
            MitaarBuddy.SinuhCorpse = false;
            Chat.WriteLine("Entering Mitaar");
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._entrance) < 20)
            {
                if (!MitaarBuddy.NavMeshMovementController.IsNavigating)
                {
                    MitaarBuddy.NavMeshMovementController.SetDestination(Constants._entrance);
                }
            }
        }
    }
}