﻿using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace MitaarBuddy
{
    public class FightState : IState
    {
        bool TeamReady;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.XanHubId || Playfield.Name == "Central Gateway")
            {
                return new IdleState();
            }
            else
            {
                if (MitaarBuddy._settings["Farming"].AsBool())
                {
                    if (Extensions.CanProceed())
                    {
                        var _alienCoccoon = DynelManager.NPCs.Where(c => c.Health > 0 && c.Name == "Alien Coccoon").ToList();

                        var _xanSpirits = DynelManager.NPCs.Where(c => c.Health > 0 && c.Name == "Xan Spirit").ToList();

                        if (MitaarBuddy.SinuhCorpse && _xanSpirits.Count == 0 && _alienCoccoon.Count == 0)
                        {
                            return new FarmingState();
                        }
                    }
                }
            }
            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Starting Hardcore Parkour!");
            TeamReady = false;
            MovementController.Instance.SetDestination(Constants._redPodium);
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (!Team.IsInTeam || Game.IsZoning) { return; }

            if (Playfield.ModelIdentity.Instance != Constants.MitaarId) { return; }

            var _sinuh = DynelManager.NPCs.FirstOrDefault(c => c.Health > 0 && c.Name == "Technomaster Sinuh");

            var _alienCoccoon = DynelManager.NPCs.Where(c => c.Health > 0 && c.Name == "Alien Coccoon")
                .OrderBy(d => DynelManager.LocalPlayer.Position.DistanceFrom(d.Position)).ToList();

            var _sinuhCorpse = DynelManager.Corpses.Where(c => c.Name == "Remains of Technomaster Sinuh").FirstOrDefault();

            var AltaroftheLight = DynelManager.AllDynels.Where(a => a.Name == "Altar of the Light").FirstOrDefault();
            var AltaroftheOutsider = DynelManager.AllDynels.Where(a => a.Name == "Altar of the Outsider").FirstOrDefault();
            var AltaroftheSource = DynelManager.AllDynels.Where(a => a.Name == "Altar of the Source").FirstOrDefault();
            var AltaroftheTrueBlood = DynelManager.AllDynels.Where(a => a.Name == "Altar of the True Blood").FirstOrDefault();

            if (_sinuhCorpse != null)
            {
                MitaarBuddy.SinuhCorpse = true;
            }

            if (!Team.Members.Any(t => t.Character == null))
            {
                TeamReady = true;
            }

            if (TeamReady)
            {
                if (DynelManager.NPCs.Where(c => c.Health > 0 && c.Name == "Xan Spirit").Any())
                {
                    if (MitaarBuddy._settings["StopAttack"].AsBool())
                    {
                        if (DynelManager.LocalPlayer.IsAttacking == true)
                        {
                            DynelManager.LocalPlayer.StopAttack(false);
                        }
                    }

                    if (MitaarBuddy._settings["Red"].AsBool())
                    {
                        HandleSpirits(AltaroftheTrueBlood, MitaarBuddy.SpiritNanos.BlessingofTheBlood);
                    }

                    if (MitaarBuddy._settings["Blue"].AsBool())
                    {
                        HandleSpirits(AltaroftheSource, MitaarBuddy.SpiritNanos.BlessingofTheSource);
                    }

                    if (MitaarBuddy._settings["Green"].AsBool())
                    {
                        HandleSpirits(AltaroftheOutsider, MitaarBuddy.SpiritNanos.BlessingofTheOutsider);
                    }

                    if (MitaarBuddy._settings["Yellow"].AsBool())
                    {
                        HandleSpirits(AltaroftheLight, MitaarBuddy.SpiritNanos.BlessingofTheLight);
                    }
                }
                else if (_alienCoccoon.Any())
                {
                    HandleAttack(_alienCoccoon.FirstOrDefault());
                }
                else if (_sinuh != null)
                {
                    HandleAttack(_sinuh);
                }
            }
        }
        void HandleSpirits(Dynel altar, int spell)
        {
            var localplayer = DynelManager.LocalPlayer;

            if (altar != null)
            {
                if (!localplayer.Buffs.Contains(spell))
                {
                    HandleSpiritPathing(altar);
                }
                else
                {
                    if (localplayer.Buffs.Find(spell, out Buff buff) && buff.RemainingTime < 3)
                    {
                        HandleSpiritPathing(altar);
                    }
                }
            }
        }

        void HandleSpiritPathing(Dynel altar)
        {
            if (DynelManager.LocalPlayer.Position.DistanceFrom(altar.Position) > 0.9f)
            {
                if (!MovementController.Instance.IsNavigating)
                {
                    MovementController.Instance.SetDestination(altar.Position);
                }
            }
        }

        void HandleAttack(SimpleChar target)
        {
            if (DynelManager.LocalPlayer.IsAttacking == true)
            {
                if (DynelManager.LocalPlayer.FightingTarget?.Identity != target.Identity)
                {
                    DynelManager.LocalPlayer.StopAttack(false);
                }
            }
            else
            {
                if (!DynelManager.LocalPlayer.IsAttackPending)
                {
                    DynelManager.LocalPlayer.Attack(target, false);
                }
            }
        }
    }
}