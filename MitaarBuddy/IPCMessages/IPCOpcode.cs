﻿namespace MitaarBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        StartStop = 1001,
        LeaderInfo = 1003,
        Farming = 1005,
        SettingsUpdate = 1006,
    }
}
