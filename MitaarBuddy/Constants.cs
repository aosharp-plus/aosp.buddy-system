﻿using AOSharp.Common.GameData;

namespace MitaarBuddy
{
    public static class Constants
    {
        public static Vector3 _reclaim = new Vector3(610.0f, 309.8f, 519.8f);

        public static Vector3 _entrance = new Vector3(347.2695, 310.8148, 407.7648);
        public static Vector3 _reneterPos = new Vector3(353.2f, 310.9f, 409.3f);

        public static Vector3 _startPosition = new Vector3(91.3f, 12.1f, 110.2f);
        public static Vector3 _greenPodium = new Vector3(108.6f, 12.1f, 110.3f);
        public static Vector3 _redPodium = new Vector3(91.3f, 12.1f, 110.2f);
        public static Vector3 _bluePodium = new Vector3(92.2f, 12.1f, 97.8f);
        public static Vector3 _yellowPodium = new Vector3(108.7f, 12.1f, 97.6f);

        //Beacon
        public static Vector3 _strangeAlienDevice = new Vector3(99.9f, 11.7f, 108.3f);

        public const int MitaarId = 6017;

        public const int XanHubId = 6013;
    }
}
