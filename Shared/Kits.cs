﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Linq;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using System.Collections.Generic;

namespace Shared
{
    public class Kits
    {
        public void SitAndUseKit(int nanoThreshold, int healthThreshold)
        {
            var localPlayer = DynelManager.LocalPlayer;

            if ((localPlayer.NanoPercent < nanoThreshold || localPlayer.HealthPercent < healthThreshold) && !InCombat() && !Casting()
                && !localPlayer.Cooldowns.ContainsKey(Stat.Treatment) && CanUseSitKit() && !localPlayer.IsFalling && !localPlayer.IsMoving)
            {
                if (localPlayer.MovementState != MovementState.Sit && !localPlayer.IsMoving)
                {
                    MovementController.Instance.SetMovement(MovementAction.SwitchToSit);
                }
                else
                {
                    UseKit();
                }
            }

            if ((localPlayer.NanoPercent >= nanoThreshold && localPlayer.HealthPercent >= healthThreshold) || InCombat() || localPlayer.Cooldowns.ContainsKey(Stat.Treatment)
               || Casting() || localPlayer.IsFalling)
            {
                if (localPlayer.MovementState == MovementState.Sit && !localPlayer.IsMoving)
                {
                    MovementController.Instance.SetMovement(MovementAction.LeaveSit);
                }
            }
        }

        public bool CanUseSitKit()
        {
            if (Inventory.Find(297274, out Item premSitKit))
            {
                return true;
            }

            List<Item> sitKits = Inventory.FindAll("Health and Nano Recharger").Where(c => c.Id != 297274).ToList();

            if (sitKits != null)
            {
                if (sitKits.Any())
                {
                    return sitKits.OrderBy(x => x.QualityLevel).Any(sitKit => MeetsSkillRequirement(sitKit));
                }
            }

            return false;
        }

        public void UseKit()
        {
            Item kit = Inventory.Items.FirstOrDefault(x => RelevantItems.Kits.Contains(x.Id));

            if (kit != null)
            {
                if (!Item.HasPendingUse)
                {
                    kit.Use(DynelManager.LocalPlayer, true);
                }
            }  
        }

        public bool MeetsSkillRequirement(Item sitKit)
        {
            var localPlayer = DynelManager.LocalPlayer;

            int skillReq = sitKit.QualityLevel > 200 ? (sitKit.QualityLevel % 200 * 3) + 1501 : (int)(sitKit.QualityLevel * 7.5f);

            return localPlayer.GetStat(Stat.FirstAid) >= skillReq || localPlayer.GetStat(Stat.Treatment) >= skillReq;
        }

        public static bool InCombat()
        {
            var localPlayer = DynelManager.LocalPlayer;

            if (Team.IsInTeam)
            {
                return Team.Members.Any(m => m.Character != null && m.Character.IsAttacking) ||
                       DynelManager.NPCs.Any(npc => npc.FightingTarget != null &&
                                                    Team.Members.Select(m => m.Identity).Contains(npc.FightingTarget.Identity));
            }

            return localPlayer.IsAttacking ||
                   (localPlayer.Pets != null && localPlayer.Pets.Any(pet => pet.Character != null && pet.Character.IsAttacking)) ||
                   DynelManager.NPCs.Any(npc => npc.FightingTarget != null &&
                                                (npc.FightingTarget.Identity == localPlayer.Identity ||
                                                 (localPlayer.Pets != null && localPlayer.Pets.Any(pet => pet.Character != null && npc.FightingTarget.Identity == pet.Character.Identity))));
        }

        public static bool Casting()
        {
            return Spell.HasPendingCast;
        }
    }

    public static class RelevantItems
    {
        public static readonly int[] Kits = { 297274, 293297, 293296, 291084, 291083, 292256, 291082 };
    }

}