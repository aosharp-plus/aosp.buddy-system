﻿using AOSharp.Common.GameData.UI;
using AOSharp.Core.UI;
using System;

namespace Buddy.Shared.UI
{
    public class InfoWindow : AOSharpWindow
    {
        public View Root;
        private string _infoText;

        public InfoWindow(string windowName, string path, string infoText, WindowStyle windowStyle = WindowStyle.Default, WindowFlags flags = WindowFlags.AutoScale | WindowFlags.NoFade) : base(windowName, path, windowStyle, flags)
        {
            _infoText = infoText;
        }

        protected override void OnWindowCreating()
        {
            Window.FindView("Info", out TextView InfoTextView);
            InfoTextView.Text = _infoText;
        }
    }
}