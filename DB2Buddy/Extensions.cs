﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System.Linq;

namespace DB2Buddy
{
    public static class Extensions
    {
        public static bool CanProceed()
        {
            return DynelManager.LocalPlayer.HealthPercent > 65 && DynelManager.LocalPlayer.NanoPercent > 65
                && DynelManager.LocalPlayer.GetStat(Stat.TemporarySkillReduction) <= 1
                && DynelManager.LocalPlayer.MovementState != MovementState.Sit
                && !Spell.HasPendingCast
                && !Debuffed();
        }

        public static bool Debuffed()
        {
            var buffs = DynelManager.LocalPlayer.Buffs;

            return buffs.Contains(DB2Buddy.Nanos.SeismicActivity)
                || buffs.Contains(DB2Buddy.Nanos.SpatialDisplacement)
                || buffs.Contains(DB2Buddy.Nanos.MachineShockwave)
                || buffs.Contains(DB2Buddy.Nanos.ActivatingtheMachine)
                || buffs.Contains(DB2Buddy.Nanos.Stunned)
                || buffs.Contains(DB2Buddy.Nanos.TheMachineGrindstoLife);
        }
    }
}
