﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DB2Buddy
{
    public class FightTowerState : IState
    {
        public static Dictionary<Vector3, string> _towerPOS = new Dictionary<Vector3, string>();

        public IState GetNextState()
        {
            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance != Constants.DB2Id)
                {
                    return new IdleState();
                }

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    if (DB2Buddy._taggedNotum || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.MachineShockwave))
                    {
                        return new NotumState();
                    }

                    if (DB2Buddy._redTower == null && DB2Buddy._blueTower == null && _towerPOS.Count == 0)
                    {
                        if (DB2Buddy._aune != null && !DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients)
                         && !DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                        {
                            DB2Buddy._redTowerBool = false;
                            DB2Buddy._blueTowerBool = false;
                            return new FightState();
                        }
                        else
                        {
                            SMovementController.SetNavDestination(Constants._startPosition);
                        }
                    }

                    if (DynelManager.LocalPlayer.IsFalling && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.first) < 75)
                    {
                        return new FellState();
                    }

                    if ((DB2Buddy.AuneCorpse || DB2Buddy._exitBeacon != null)
                            && DB2Buddy._settings["Farming"].AsBool())
                    {
                        return new FarmingState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            SMovementController.Halt();

            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack(false);
            }
            Chat.WriteLine($"Tower!");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (DynelManager.LocalPlayer.IsAttacking)
            {
                DynelManager.LocalPlayer.StopAttack(false);
            }

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                if (Extensions.Debuffed())
                {
                    if (DynelManager.LocalPlayer.IsAttacking == true)
                    {
                        DynelManager.LocalPlayer.StopAttack(false);
                    }

                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }
                }
                else
                {
                    if (DB2Buddy._redTower != null)
                    {
                        if (DB2Buddy._redTower.IsInLineOfSight && DB2Buddy._redTower.IsInAttackRange(true))
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                            else
                            {
                                if (DynelManager.LocalPlayer.FightingTarget == DB2Buddy._redTower)
                                {
                                    DynelManager.LocalPlayer.StopAttack(false);
                                }

                                if (DynelManager.LocalPlayer.FightingTarget == null && !DynelManager.LocalPlayer.IsAttackPending)
                                {
                                    if (_towerPOS.ContainsKey(DB2Buddy._redTower.Position))
                                    {
                                        _towerPOS.Remove(DB2Buddy._redTower.Position);
                                    }

                                    DynelManager.LocalPlayer.Attack(DB2Buddy._redTower, false);
                                }
                            }
                        }
                        else
                        {
                            if (!DB2Buddy._redTower.IsInLineOfSight || !DB2Buddy._redTower.IsInAttackRange(true))
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    if (!_towerPOS.ContainsKey(DB2Buddy._redTower.Position))
                                    {
                                        _towerPOS[DB2Buddy._redTower.Position] = DB2Buddy._redTower.Name;
                                    }

                                    SMovementController.SetNavDestination(DB2Buddy._redTower.Position);
                                }
                            }
                        }
                    }
                    else if (DB2Buddy._blueTower != null)
                    {
                        if (!DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                        {
                            if (DB2Buddy._blueTower.IsInLineOfSight && DB2Buddy._blueTower.IsInAttackRange(true))
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                                else
                                {
                                    if (DynelManager.LocalPlayer.FightingTarget == DB2Buddy._blueTower)
                                    {
                                        DynelManager.LocalPlayer.StopAttack(false);
                                    }

                                    if (DynelManager.LocalPlayer.FightingTarget == null && !DynelManager.LocalPlayer.IsAttackPending)
                                    {
                                        if (_towerPOS.ContainsKey(DB2Buddy._blueTower.Position))
                                        {
                                            _towerPOS.Remove(DB2Buddy._blueTower.Position);
                                        }
                                        DynelManager.LocalPlayer.Attack(DB2Buddy._blueTower, false);
                                    }
                                }
                            }
                            else
                            {
                                if (!DB2Buddy._blueTower.IsInLineOfSight || !DB2Buddy._blueTower.IsInAttackRange(true))
                                {
                                    if (!SMovementController.IsNavigating())
                                    {
                                        if (!_towerPOS.ContainsKey(DB2Buddy._blueTower.Position))
                                        {
                                            _towerPOS[DB2Buddy._blueTower.Position] = DB2Buddy._blueTower.Name;
                                        }

                                        SMovementController.SetNavDestination(DB2Buddy._blueTower.Position);
                                    }
                                }
                            }
                        }
                    }
                    else if (_towerPOS.Count >= 1)
                    {
                        Vector3 towerPosition = _towerPOS.Keys.First();

                        if (DynelManager.LocalPlayer.Position.DistanceFrom(towerPosition) > 5f)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(towerPosition);
                            }
                        }
                        else
                        {
                            if (_towerPOS.ContainsKey(towerPosition))
                            {
                                _towerPOS.Remove(towerPosition);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}