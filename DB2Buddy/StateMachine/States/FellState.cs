﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace DB2Buddy
{
    public class FellState : IState
    {
        public IState GetNextState()
        {
            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance != Constants.DB2Id)
                {
                    return new IdleState();
                }
                else
                {
                    if (!DynelManager.LocalPlayer.IsFalling)
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._warpPos) < 10f)
                        {
                            return new PathToBossState();
                        }

                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPosition) < 30)
                        {
                            return new FightState();
                        }
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("You fell, Dumbass!");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    if (DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.PathtoElevation3))
                    {
                        SMovementController.SetNavDestination(Constants.forth);
                    }
                    else if (DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.PathtoElevation2))
                    {
                        SMovementController.SetNavDestination(Constants.third);
                    }
                    else if (DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.PathtoElevation1))
                    {
                        SMovementController.SetNavDestination(Constants.second);
                    }
                    else
                    {
                        SMovementController.SetNavDestination(Constants.first);
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}