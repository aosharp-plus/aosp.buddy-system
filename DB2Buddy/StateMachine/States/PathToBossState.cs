﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace DB2Buddy
{
    public class PathToBossState : IState
    {
       
        public IState GetNextState()
        {
            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance != Constants.DB2Id)
                {
                    return new IdleState();
                }
                else
                {
                    DB2Buddy.GetDynels();

                    if (DB2Buddy._taggedNotum || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.MachineShockwave))
                    {
                        return new NotumState();
                    }

                    if (DB2Buddy._aune != null)
                    {
                        if (DB2Buddy._redTower != null || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                        {
                            return new FightTowerState();
                        }

                        if (DB2Buddy._blueTower != null || DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients))
                        {
                            if (!DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                            {
                                return new FightTowerState();
                            }
                        }
                    }

                    if (DB2Buddy._aune != null && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._startPosition) < 1)
                    {
                        return new FightState();
                    }

                    if (DB2Buddy._aune == null && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._startPosition) < 5)
                    {
                        return new IdleState();
                    }

                    if (DynelManager.LocalPlayer.IsFalling && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.first) < 75)
                    {
                        return new FellState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants._centerPosition) > 5f)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants._startPosition);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}