﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace DB2Buddy
{
    public class FarmingState : IState
    {
        public static bool _atCorpse = false;
        private bool _disband = false;
        private static double _timeToDisband;

        public IState GetNextState()
        {
            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance == Constants.PWId)
                {
                    if (DynelManager.LocalPlayer.Identity == DB2Buddy.Leader)
                    {
                        return new ReformState();
                    }
                    else
                    {
                        return new IdleState();
                    }
                }

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    if (DynelManager.LocalPlayer.IsFalling && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.first) < 75)
                    {
                        return new FellState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Pillage me treasures!");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            _disband = false;
            _atCorpse = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                if (DB2Buddy._auneCorpse != null)
                {
                    if (!DynelManager.LocalPlayer.Buffs.Contains(NanoLine.Stun))
                    {
                        if (!MovementController.Instance.IsNavigating)
                        {
                            if (!_atCorpse)
                            {
                                if (DynelManager.LocalPlayer.Position.DistanceFrom(DB2Buddy._auneCorpse.Position) < 1.0f)
                                {
                                    if (DynelManager.LocalPlayer.Identity == DB2Buddy.Leader)
                                    {
                                        Chat.WriteLine("Pause for looting, 30 sec");
                                        _timeToDisband = Time.AONormalTime + 30;
                                        _disband = true;
                                    }

                                    _atCorpse = true;
                                }
                                else
                                {
                                    SMovementController.SetNavDestination(DB2Buddy._auneCorpse.Position);
                                }
                            }
                        }
                    }
                }
                if (DynelManager.LocalPlayer.Identity == DB2Buddy.Leader && _atCorpse && Team.IsInTeam)
                {
                    Disband();
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }

        private void Disband()
        {
            if (_disband && Time.AONormalTime >= _timeToDisband)
            {
                Chat.WriteLine("Done, Disbanding");
                Team.Disband();

                _disband = false;
            }
        }
    }
}