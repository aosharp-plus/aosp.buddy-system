﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.Linq;

namespace DB2Buddy
{
    public class NotumState : IState
    {
        private double _timeToTagReset;

        public IState GetNextState()
        {
            if (DB2Buddy._settings["Enable"].AsBool())
            {
                if (Playfield.ModelIdentity.Instance != Constants.DB2Id)
                {
                    return new IdleState();
                }

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    DB2Buddy.GetDynels();

                    if (!DB2Buddy._taggedNotum && !DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.MachineShockwave))
                    {
                        if (DB2Buddy._redTower == null && DB2Buddy._blueTower == null)
                        {
                            if (DB2Buddy._aune != null && !DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients)
                             && !DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                            {
                                return new FightState();
                            }
                        }

                        if (DB2Buddy._aune != null)
                        {
                            if (DB2Buddy._redTower != null || DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                            {
                                return new FightTowerState();
                            }

                            if (DB2Buddy._blueTower != null || (DB2Buddy._aune != null && DB2Buddy._aune.Buffs.Contains(DB2Buddy.Nanos.StrengthOfTheAncients)))
                            {
                                if (!DynelManager.LocalPlayer.Buffs.Contains(DB2Buddy.Nanos.XanBlessingoftheEnemy))
                                {
                                    return new FightTowerState();
                                }
                            }
                        }
                        else
                        {
                            return new IdleState();
                        }
                    }
                    if (DynelManager.LocalPlayer.IsFalling && DynelManager.LocalPlayer.Position.DistanceFrom(Constants.first) < 75)
                    {
                        return new FellState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Nuke!");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            DB2Buddy._taggedNotum = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                if (Extensions.Debuffed())
                {
                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }

                    if (DB2Buddy._notumIrregularity != null)
                    {
                        if (DynelManager.LocalPlayer.Position.Distance2DFrom(DB2Buddy._notumIrregularity.Position) > 0.5)
                        {
                            DynelManager.LocalPlayer.Position = DB2Buddy._notumIrregularity.Position;
                            MovementController.Instance.SetMovement(MovementAction.Update);
                            MovementController.Instance.SetMovement(MovementAction.TurnLeftStart);
                            MovementController.Instance.SetMovement(MovementAction.TurnLeftStop);
                            MovementController.Instance.SetMovement(MovementAction.Update);
                        }
                    }
                }
                else
                {
                    if (DB2Buddy._notumIrregularity != null)
                    {
                        var playerDistance = DynelManager.LocalPlayer.Position.DistanceFrom(DB2Buddy._notumIrregularity.Position);

                        if (playerDistance > 0.5)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(DB2Buddy._notumIrregularity.Position);
                            }
                        }

                        if (playerDistance < 0.6)
                        {
                            if (SMovementController.IsNavigating())
                            {
                                SMovementController.Halt();
                            }
                            else
                            {
                                if (_timeToTagReset <= 0)
                                {
                                    _timeToTagReset = Time.AONormalTime + 5;
                                }
                                else if (Time.AONormalTime >= _timeToTagReset)
                                {
                                    DB2Buddy._taggedNotum = false;
                                    _timeToTagReset = 0;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants._centerPosition);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}