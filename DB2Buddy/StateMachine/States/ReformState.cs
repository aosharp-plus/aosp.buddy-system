﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Collections.Generic;
using System.Linq;

namespace DB2Buddy
{
    public class ReformState : IState
    {
        public static ReformPhase _phase;

        public static List<Identity> _teamCache = new List<Identity>();
        List<Identity> _invitedList = new List<Identity>();

        public IState GetNextState()
        {
            if (_phase == ReformPhase.Completed)
            {
                return new EnterState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Reforming");

            _phase = ReformPhase.Inviting;
        }

        public void OnStateExit()
        {
            _invitedList.Clear();
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (DynelManager.LocalPlayer.Identity == DB2Buddy.Leader)
            {
                if (_phase == ReformPhase.Disbanding)
                {
                    if (Team.IsInTeam)
                    {
                        if (!Team.Members.Any(t => t.Character == null))
                        {
                            Team.Disband();
                        }
                    }
                    else
                    {
                        _phase = ReformPhase.Inviting;
                    }
                }
                else if (_phase == ReformPhase.Inviting)
                {
                    if (Team.IsInTeam)
                    {
                        if (_teamCache.Count == Team.Members.Count)
                        {
                            _phase = ReformPhase.Completed;
                        }
                    }

                    foreach (SimpleChar player in DynelManager.Players.Where(c => c.IsInPlay && !_invitedList.Contains(c.Identity) && _teamCache.Contains(c.Identity)))
                    {
                        if (_invitedList.Contains(player.Identity)) { continue; }

                        _invitedList.Add(player.Identity);

                        if (player.Identity == DB2Buddy.Leader) { continue; }

                        Team.Invite(player.Identity);
                    }
                }
            }
        }
    }

    public enum ReformPhase
    {
        Disbanding,
        Inviting,
        Waiting,
        Completed
    }
}
