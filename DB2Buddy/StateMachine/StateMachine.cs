﻿using AOSharp.Core.UI;
using System;

namespace DB2Buddy
{
    public class StateMachine
    {
        public IState CurrentState { get; private set; } = null;

        public StateMachine(IState defaultState)
        {
            SetState(defaultState);
        }

        public void Tick()
        {
            try
            {
                IState nextState = CurrentState.GetNextState();

                if (nextState != null)
                {
                    SetState(nextState);
                }

                CurrentState.Tick();

            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + DB2Buddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != DB2Buddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    DB2Buddy.previousErrorMessage = errorMessage;
                }
            }
        }
        public void SetState(IState state, bool triggerEvents = true)
        {
            if (CurrentState != null && triggerEvents)
            {
                CurrentState.OnStateExit();
            }

            CurrentState = state;

            if (triggerEvents)
            {
                state.OnStateEnter();
            }
        }
    }
}
