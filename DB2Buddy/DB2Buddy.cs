﻿using System;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using AOSharp.Core.IPC;
using AOSharp.Common.GameData.UI;
using DB2Buddy.IPCMessages;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using SmokeLounge.AOtomation.Messaging.Messages.ChatMessages;
using SmokeLounge.AOtomation.Messaging.Messages;
using System.Linq;

namespace DB2Buddy
{
    public class DB2Buddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static IPCChannel IPCChannel { get; private set; }
        public static Config Config { get; private set; }

        public static bool Enable = false;
        public static bool Farming = false;

        public static bool _init = false;
        public static bool _initLol = false;
        public static bool _initStart = false;
        public static bool _initTower = false;
        public static bool _initCorpse = false;
        public static bool IsLeader = false;
        public static bool _repeat = false;

        public static bool _taggedNotum = false;
        public static bool _redTowerBool = false;
        public static bool _blueTowerBool = false;

        public static bool AuneCorpse = false;

        public static double _time = Time.AONormalTime;

        public static Identity Leader = Identity.None;
        public static SimpleChar _leader;

        public static SimpleChar _aune;
        public static SimpleChar _redTower;
        public static SimpleChar _blueTower;
        public static Dynel _exitBeacon;
        public static Dynel _notumIrregularity;
        public static Corpse _auneCorpse;

        public static string PluginDirectory;

        public static Window _infoWindow;

        public static Settings _settings;

        public static List<Identity> _teamCache = new List<Identity>();

        public static List<Vector3> _mistLocations = new List<Vector3>();

        public static string PluginDir;

        public static string previousErrorMessage = string.Empty;

        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("DB2Buddy");

                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{CommonParameters.BasePath}\\{CommonParameters.AppPath}\\DB2Buddy\\{DynelManager.LocalPlayer.Name}\\Config.json");

                SMovementControllerSettings mSettings = new SMovementControllerSettings
                {
                    NavMeshSettings = new SNavMeshSettings { DrawNavMesh = false, DrawDistance = 30 },

                    PathSettings = new SPathSettings
                    {
                        DrawPath = true,
                        MinRotSpeed = 10,
                        MaxRotSpeed = 30,
                        UnstuckUpdate = 5000,
                        UnstuckThreshold = 2f,
                        RotUpdate = 10,
                        MovementUpdate = 200,
                        PathRadius = 0.28f,
                        Extents = new Vector3(1.0f, 1.0f, 1.0f)
                    }
                };

                SMovementController.Set(mSettings);

                SMovementController.AutoLoadNavmeshes($"{PluginDir}\\NavMeshes");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.StartStop, OnStartStopMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Farming, OnFarmingStatusMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.Enter, OnEnterMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.LeaderInfo, OnLeaderInfoMessage);

                Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannelChangedEvent += IPCChannel_Changed;

                SettingsController.RegisterSettingsWindow("DB2Buddy", pluginDir + "\\UI\\DB2BuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                if (Game.IsNewEngine)
                {
                    Chat.WriteLine("Does not work on this engine!");
                }
                else
                {
                    Chat.WriteLine("DB2Buddy Loaded!");
                    Chat.WriteLine("/buddy for settings. /enable to start and stop.");
                }

                _settings.AddVariable("Enable", false);
                _settings.AddVariable("Farming", false);

                _settings["Enable"] = false;

                Chat.RegisterCommand("enable", BuddyCommand);

                Game.OnUpdate += OnUpdate;
                Network.ChatMessageReceived += BossShouts;
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }
        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        public static void Start()
        {
            Enable = true;

            Chat.WriteLine("Db2Buddy enabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }
        }

        private void Stop()
        {
            Enable = false;

            Chat.WriteLine("Db2Buddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }

            SMovementController.Halt();
        }

        private void FarmingEnabled()
        {
            Chat.WriteLine("Farming Enabled.");
            Farming = true;
        }
        private void FarmingDisabled()
        {
            Chat.WriteLine("Farming Disabled");
            Farming = false;
        }

        private void OnEnterMessage(int sender, IPCMessage msg)
        {
            if (IsLeader) { return; }

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }
        }

        private void OnStartStopMessage(int sender, IPCMessage msg)
        {
            if (msg is StartStopIPCMessage startStopMessage)
            {
                if (startStopMessage.IsStarting)
                {
                    _settings["Enable"] = true;
                    Start();
                }
                else
                {
                    _settings["Enable"] = false;
                    Stop();
                }
            }
        }

        private void OnFarmingStatusMessage(int sender, IPCMessage msg)
        {
            if (msg is FarmingStatusMessage farmingStatusMessage)
            {
                if (farmingStatusMessage.IsFarming)
                {
                    _settings["Farming"] = true;
                    FarmingEnabled();
                }
                else
                {
                    _settings["Farming"] = false;
                    FarmingDisabled();
                }
            }
        }

        private void OnLeaderInfoMessage(int sender, IPCMessage msg)
        {
            if (msg is LeaderInfoIPCMessage leaderInfoMessage)
            {
                if (leaderInfoMessage.IsRequest)
                {
                    if (Team.IsLeader)
                    {
                        IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader, IsRequest = false });
                    }
                }
                else
                {
                    Leader = leaderInfoMessage.LeaderIdentity;
                }
            }
        }

        private void HandleInfoViewClick(object s, ButtonBase button)
        {
            _infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\DB2BuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            _infoWindow.Show(true);
        }

        private void OnUpdate(object s, float deltaTime)
        {
            try
            {
                if (Game.IsZoning) { return; }

                _stateMachine.Tick();

                if (Playfield.ModelIdentity.Instance == Constants.DB2Id)
                {
                    GetDynels();
                }
                else
                {
                    Shared.Kits kitsInstance = new Shared.Kits();

                    kitsInstance.SitAndUseKit(66, 66);
                }

                if (Team.IsInTeam)
                {
                    if (Leader == Identity.None)
                    {
                        if (Team.IsLeader)
                        {
                            Leader = DynelManager.LocalPlayer.Identity;
                        }
                        else
                        {
                            IPCChannel.Broadcast(new LeaderInfoIPCMessage() { IsRequest = true });
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Identity == Leader)
                        {
                            foreach (TeamMember member in Team.Members)
                            {
                                if (ReformState._teamCache.Count <= 5)
                                {
                                    if (!ReformState._teamCache.Contains(member.Identity))
                                    {
                                        ReformState._teamCache.Add(member.Identity);
                                    }
                                }
                            }
                        }
                    }
                }

                #region UI

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

                    if (channelInput != null)
                    {
                        if (int.TryParse(channelInput.Text, out int channelValue)
                            && Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel != channelValue)
                        {
                            Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel = channelValue;
                        }
                    }

                    if (SettingsController.settingsWindow.FindView("DB2BuddyInfoView", out Button infoView))
                    {
                        infoView.Tag = SettingsController.settingsWindow;
                        infoView.Clicked = HandleInfoViewClick;
                    }

                    if (!_settings["Enable"].AsBool() && Enable)
                    {
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                    if (_settings["Enable"].AsBool() && !Enable)
                    {
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }

                    if (!_settings["Farming"].AsBool() && Farming)
                    {
                        IPCChannel.Broadcast(new FarmingStatusMessage { IsFarming = false });
                        FarmingDisabled();
                    }
                    if (_settings["Farming"].AsBool() && !Farming)
                    {
                        IPCChannel.Broadcast(new FarmingStatusMessage { IsFarming = true });
                        FarmingEnabled();
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        public static void GetDynels()
        {
            _aune = DynelManager.NPCs
                  .Where(c => c.Health > 0
                      && c.Name == "Ground Chief Aune")
            .FirstOrDefault();

            _redTower = DynelManager.NPCs
              .Where(c => c.Health > 0
                  && c.Name.Contains("Strange Xan Artifact")
                  && c.Buffs.Contains(274119))
              .FirstOrDefault();

            _blueTower = DynelManager.NPCs
               .Where(c => c.Health > 0
                   && c.Name.Contains("Strange Xan Artifact")
                   && !c.Buffs.Contains(274119))
               .FirstOrDefault();

            _exitBeacon = DynelManager.AllDynels
                .Where(c => c.Name.Contains("Dust Brigade Exit Beacon"))
                .FirstOrDefault();

            _notumIrregularity = DynelManager.AllDynels
                  .Where(c => c.Name == "Notum Irregularity")
                  .FirstOrDefault();

            _auneCorpse = DynelManager.Corpses
               .Where(c => c.Name == "Remains of Ground Chief Aune")
               .FirstOrDefault();
        }

        public static void BossShouts(object s, ChatMessageBody n3Msg)
        {
            if (n3Msg.PacketType == ChatMessageType.NpcMessage)
            {
                NpcMessage npcMsg = (NpcMessage)n3Msg;

                string[] redTowerMsg = new string[2] { "Shoot me cowards", "Go ahead!  Shoot me" };
                string[] blueTowerMsg = new string[1] { "You will never know the secrets of the machine" };

                if (redTowerMsg.Any(x => npcMsg.Text.Contains(x)))
                {
                    _redTowerBool = true;
                }

                if (blueTowerMsg.Any(x => npcMsg.Text.Contains(x)))
                {
                    _blueTowerBool = true;
                }
            }
        }
        private void BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Enable"].AsBool())
                    {
                        _settings["Enable"] = true;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }
                    else
                    {
                        _settings["Enable"] = false;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                }
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
        public static class Nanos
        {
            public const int XanBlessingoftheEnemy = 274101;//He reached out to his enemy and granted him strength and life
            //public const int XanBlessingoftheEnemy = 274117; //He reached out to his enemy and stoked the fire of his life.
            public const int StrengthOfTheAncients = 273220;

            public const int NotumPull = 274359;

            public const int SeismicActivity = 270742;
            public const int ActivatingtheMachine = 274200;
            public const int MachineShockwave = 274350;
            public const int SpatialDisplacement = 274227;
            public const int TheMachineGrindstoLife = 274219;
            public const int Stunned = 275145;


            public const int PathtoElevation1 = 277947;
            public const int PathtoElevation2 = 277958;
            public const int PathtoElevation3 = 277959;
            public const int PathtoElevation4 = 277952;
        }

        public static int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;

            var lineMatch = Regex.Match(ex.StackTrace ?? "", @":line (\d+)$", RegexOptions.Multiline);

            if (lineMatch.Success)
            {
                lineNumber = int.Parse(lineMatch.Groups[1].Value);
            }

            return lineNumber;
        }
    }
}
