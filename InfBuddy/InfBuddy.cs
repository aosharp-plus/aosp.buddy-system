﻿using AOSharp.Common.GameData;
using AOSharp.Common.GameData.UI;
using AOSharp.Core;
using AOSharp.Core.IPC;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using InfBuddy.IPCMessages;
using SharpNav;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace InfBuddy
{
    public class InfBuddy : AOPluginEntry
    {
        private StateMachine _stateMachine;

        public static SMovementController SMovementController { get; set; }

        public static IPCChannel IPCChannel { get; set; }

        public static Config Config { get; private set; }

        public static bool Enable = false;

        //public static bool Ready = true;
        public static double missionTimer;
        public static double missionTimeOut;

        private Dictionary<Identity, bool> teamReadiness = new Dictionary<Identity, bool>();
        //private bool? lastSentIsReadyState = null;

        ModeSelection currentMode;
        FactionSelection currentFaction;
        DifficultySelection currentDifficulty;

        public static Identity Leader = Identity.None;

        public static bool DoubleReward;

        public static double _stateTimeOut;

        public static string previousErrorMessage = string.Empty;

        public static List<string> _namesToIgnore = new List<string>
        {
                    "One Who Obeys Precepts",
                    "Buckethead Technodealer",
                    "The Retainer Of Ergo",
                    "Guardian Spirit of Purification"
        };

        private static Window infoWindow;

        private static string PluginDir;

        public static Settings _settings;

        public static float distance = -1f;

        [Obsolete]
        public override void Run(string pluginDir)
        {
            try
            {
                _settings = new Settings("InfBuddy");
                PluginDir = pluginDir;

                Config = Config.Load($"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}\\{CommonParameters.BasePath}\\{CommonParameters.AppPath}\\InfBuddy\\{DynelManager.LocalPlayer.Name}\\Config.json");

                SMovementControllerSettings mSettings = new SMovementControllerSettings
                {
                    NavMeshSettings = new SNavMeshSettings { DrawNavMesh = false, DrawDistance = 30 },

                    PathSettings = new SPathSettings
                    {
                        DrawPath = true,
                        MinRotSpeed = 10,
                        MaxRotSpeed = 30,
                        UnstuckUpdate = 5000,
                        UnstuckThreshold = 2f,
                        RotUpdate = 10,
                        MovementUpdate = 200,
                        PathRadius = 0.29f,
                        Extents = new Vector3(3f, 3f, 3f)
                    }
                };

                SMovementController.Set(mSettings);

                SMovementController.AutoLoadNavmeshes($"{PluginDir}\\NavMeshes");

                IPCChannel = new IPCChannel(Convert.ToByte(Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel));

                IPCChannel.RegisterCallback((int)IPCOpcode.StartStop, OnStartStopMessage);
                IPCChannel.RegisterCallback((short)IPCOpcode.ModeSelections, OnModeSelectionsMessage);
                IPCChannel.RegisterCallback((int)IPCOpcode.LeaderInfo, OnLeaderInfoMessage);
                //IPCChannel.RegisterCallback((int)IPCOpcode.WaitAndReady, OnWaitAndReadyMessage);

                Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannelChangedEvent += IPCChannel_Changed;

                Chat.RegisterCommand("enable", BuddyCommand);

                SettingsController.RegisterSettingsWindow("InfBuddy", pluginDir + "\\UI\\InfBuddySettingWindow.xml", _settings);

                _stateMachine = new StateMachine(new IdleState());

                NpcDialog.AnswerListChanged += NpcDialog_AnswerListChanged;
                Game.OnUpdate += OnUpdate;
                Team.TeamRequest += OnTeamRequest;
                SMovementController.AgentStateChange += HandleAgentStateChange;

                _settings.AddVariable("ModeSelection", (int)ModeSelection.Normal);
                _settings.AddVariable("FactionSelection", (int)FactionSelection.Clan);
                _settings.AddVariable("DifficultySelection", (int)DifficultySelection.Hard);

                _settings.AddVariable("Enable", false);
                _settings["Enable"] = false;

                _settings.AddVariable("Stop", false);
                _settings["Stop"] = false;

                _settings.AddVariable("DoubleReward", false);
                _settings.AddVariable("Merge", false);
                _settings.AddVariable("Looting", false);
                _settings.AddVariable("Leech", false);
                _settings.AddVariable("Tank", false);

                if (!Game.IsNewEngine)
                {
                    Chat.WriteLine("InfBuddy Loaded!");
                    Chat.WriteLine("/buddy for settings. /enable to start and stop.");
                }
                else
                {
                    Chat.WriteLine("Does not work on this engine!");
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        private void HandleAgentStateChange(NavMeshQuery.AgentNavMeshState state)
        {
            switch (state)
            {
                case NavMeshQuery.AgentNavMeshState.OutNavMesh:
                    HandlePlayerOffNavMesh();
                    break;
                case NavMeshQuery.AgentNavMeshState.OutPolygon:
                    HandlePlayerOffNavMesh();
                    break;
                default:
                    break;

            }
        }

        private void HandlePlayerOffNavMesh()
        {
            Vector3 nearestNavPoint = SMovementController.GetClosestNavPoint(DynelManager.LocalPlayer.Position);

            if (nearestNavPoint != Vector3.Zero)
            {
                SMovementController.SetDestination(nearestNavPoint);
            }
            else
            {
                Chat.WriteLine("Unable to find a nearby navigation point to unstuck the character.", ChatColor.Red);
            }
        }

        public override void Teardown()
        {
            SettingsController.CleanUp();
        }

        public static void IPCChannel_Changed(object s, int e)
        {
            IPCChannel.SetChannelId(Convert.ToByte(e));
            Config.Save();
        }

        private void InfoView(object s, ButtonBase button)
        {
            infoWindow = Window.CreateFromXml("Info", PluginDir + "\\UI\\InfBuddyInfoView.xml",
                windowSize: new Rect(0, 0, 440, 510),
                windowStyle: WindowStyle.Default,
                windowFlags: WindowFlags.AutoScale | WindowFlags.NoFade);

            infoWindow.Show(true);
        }
        private void Start()
        {
            Enable = true;

            Chat.WriteLine("InfBuddy enabled.");

            if (Playfield.ModelIdentity.Instance != Constants.Mission)
            {
                foreach (Mission mission in Mission.List)
                {
                    if (mission.DisplayName.Contains("The Purification Ritual"))
                    {
                        Chat.WriteLine("Deleting old mission");
                        mission.Delete();
                    }
                }
            }

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }
        }

        private void Stop()
        {
            Enable = false;

            Chat.WriteLine("InfBuddy disabled.");

            if (!(_stateMachine.CurrentState is IdleState))
            {
                _stateMachine.SetState(new IdleState());
            }

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        private void OnStartStopMessage(int sender, IPCMessage msg)
        {
            if (msg is StartStopIPCMessage startStopMessage)
            {
                if (startStopMessage.IsStarting)
                {
                    _settings["Enable"] = true;
                    Start();
                }
                else
                {
                    _settings["Enable"] = false;
                    Stop();
                }
            }
        }

        private void OnModeSelectionsMessage(int sender, IPCMessage msg)
        {
            if (msg is ModeSelectionsIPCMessage modeSelectionsMessage)
            {
                currentMode = modeSelectionsMessage.Mode;
                currentFaction = modeSelectionsMessage.Faction;
                currentDifficulty = modeSelectionsMessage.Difficulty;

                _settings["ModeSelection"] = (int)currentMode;
                _settings["FactionSelection"] = (int)currentFaction;
                _settings["DifficultySelection"] = (int)currentDifficulty;
            }
        }
        private void OnLeaderInfoMessage(int sender, IPCMessage msg)
        {
            if (msg is LeaderInfoIPCMessage leaderInfoMessage)
            {
                if (leaderInfoMessage.IsRequest)
                {
                    if (DynelManager.LocalPlayer.Identity == Leader)
                    {
                        IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader, IsRequest = false });
                    }
                }
                else
                {
                    Leader = leaderInfoMessage.LeaderIdentity;
                }
            }
        }
        //private void OnWaitAndReadyMessage(int sender, IPCMessage msg)
        //{
        //    if (msg is WaitAndReadyIPCMessage waitAndReadyMessage)
        //    {
        //        Identity senderIdentity = waitAndReadyMessage.PlayerIdentity;

        //        teamReadiness[senderIdentity] = waitAndReadyMessage.IsReady;

        //        //Chat.WriteLine($"IPC received. Sender: {senderIdentity}, IsReady: {waitAndReadyMessage.IsReady}");

        //        bool allReady = true;

        //        foreach (var teamMember in Team.Members)
        //        {
        //            if (teamReadiness.ContainsKey(teamMember.Identity) && !teamReadiness[teamMember.Identity])
        //            {
        //                allReady = false;
        //                break;
        //            }
        //        }

        //        if (Leader == DynelManager.LocalPlayer.Identity)
        //        {
        //            Ready = allReady;
        //        }
        //    }
        //}

        private void OnUpdate(object s, float deltaTime)
        {
            try
            {
                if (Game.IsZoning) { return; }

                Shared.Kits kitsInstance = new Shared.Kits();

                kitsInstance.SitAndUseKit(66, 66);

                if (Team.IsInTeam)
                {
                    if (_settings["Merge"].AsBool())
                    {
                        if (Leader == Identity.None)
                        {
                            var teamLeader = Team.Members.FirstOrDefault(member => member.IsLeader)?.Character;

                            Leader = teamLeader?.Identity ?? Identity.None;
                        }
                    }
                    else
                    {
                        if (Leader == Identity.None)
                        {
                            if (_settings["Tank"].AsBool())
                            {
                                IPCChannel.Broadcast(new LeaderInfoIPCMessage() { LeaderIdentity = Leader });
                                Leader = DynelManager.LocalPlayer.Identity;
                            }
                            else
                            {
                                IPCChannel.Broadcast(new LeaderInfoIPCMessage() { IsRequest = true });
                            }
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.Identity == Leader)
                            {

                                foreach (var teamMember in Team.Members)
                                {
                                    var member = teamMember.Character;

                                    if (member == null) { continue; }

                                    if (!ReformState.TeamList.Contains(member.Identity))
                                    {
                                        Chat.WriteLine($"Adding {member.Name} to team list.");
                                        ReformState.TeamList.Add(member.Identity);
                                    }
                                }

                                //var itemsToRemove = new List<SimpleChar>();

                                //foreach (var member in ReformState.TeamList)
                                //{
                                //    if (!Team.Members.Any(tm => tm.Character == member))
                                //    {
                                //        itemsToRemove.Add(member);
                                //    }
                                //}

                                //foreach (var member in itemsToRemove)
                                //{
                                //    Chat.WriteLine($"Removing {member.Name} from team list.");
                                //    ReformState.TeamList.Remove(member);
                                //}

                            }
                            //else
                            //{
                            //    var localPlayer = DynelManager.LocalPlayer;
                            //    bool currentIsReadyState = true;

                            //    if (!Shared.Kits.InCombat())
                            //    {
                            //        if (Spell.HasPendingCast || localPlayer.NanoPercent < 60 || localPlayer.HealthPercent < 60)
                            //        {
                            //            currentIsReadyState = false;
                            //        }
                            //    }
                            //    else if (!Spell.HasPendingCast && localPlayer.NanoPercent > 66
                            //        && localPlayer.HealthPercent > 66)
                            //    {
                            //        currentIsReadyState = true;
                            //    }

                            //    if (currentIsReadyState != lastSentIsReadyState)
                            //    {
                            //        IPCChannel.Broadcast(new WaitAndReadyIPCMessage
                            //        {
                            //            IsReady = currentIsReadyState,
                            //            PlayerIdentity = localPlayer.Identity
                            //        });

                            //        lastSentIsReadyState = currentIsReadyState;
                            //    }
                            //}
                        }
                    }
                }

                #region UI

                if (SettingsController.settingsWindow != null && SettingsController.settingsWindow.IsValid)
                {
                    SettingsController.settingsWindow.FindView("ChannelBox", out TextInputView channelInput);

                    if (channelInput != null)
                    {
                        if (int.TryParse(channelInput.Text, out int channelValue)
                            && Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel != channelValue)
                        {
                            Config.CharSettings[DynelManager.LocalPlayer.Name].IPCChannel = channelValue;
                        }
                    }

                    if (SettingsController.settingsWindow.FindView("InfBuddyInfoView", out Button infoView))
                    {
                        infoView.Tag = SettingsController.settingsWindow;
                        infoView.Clicked = InfoView;
                    }

                    if (!_settings["Enable"].AsBool() && Enable)
                    {
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }

                    if (_settings["Enable"].AsBool() && !Enable)
                    {
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }

                    ModeSelection newMode = (ModeSelection)_settings["ModeSelection"].AsInt32();
                    FactionSelection newFaction = (FactionSelection)_settings["FactionSelection"].AsInt32();
                    DifficultySelection newDifficulty = (DifficultySelection)_settings["DifficultySelection"].AsInt32();

                    bool modeChanged = newMode != currentMode;
                    bool factionChanged = newFaction != currentFaction;
                    bool difficultyChanged = newDifficulty != currentDifficulty;

                    if (modeChanged || factionChanged || difficultyChanged)
                    {
                        ModeSelectionsIPCMessage modeSelectionsMessage = new ModeSelectionsIPCMessage
                        {
                            Mode = newMode,
                            Faction = newFaction,
                            Difficulty = newDifficulty
                        };

                        IPCChannel.Broadcast(modeSelectionsMessage);

                        if (modeChanged)
                        {
                            currentMode = newMode;
                        }

                        if (factionChanged)
                        {
                            currentFaction = newFaction;
                        }

                        if (difficultyChanged)
                        {
                            currentDifficulty = newDifficulty;
                        }
                    }
                }

                #endregion

                if (_settings["Enable"].AsBool())
                {
                    _stateMachine.Tick();
                }

            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    previousErrorMessage = errorMessage;
                }
            }
        }

        private void NpcDialog_AnswerListChanged(object s, Dictionary<int, string> options)
        {
            SimpleChar dialogNpc = DynelManager.GetDynel((Identity)s).Cast<SimpleChar>();

            if (dialogNpc.Name == "The Retainer Of Ergo")
            {
                foreach (KeyValuePair<int, string> option in options)
                {
                    if (option.Value == "Is there anything I can help you with?" ||
                        (FactionSelection.Clan == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the Unredeemed!") ||
                        (FactionSelection.Omni == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the Redeemed!") ||
                        (FactionSelection.Neutral == (FactionSelection)_settings["FactionSelection"].AsInt32() && option.Value == "I will defend against the creatures of the brink!") ||
                        (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will deal with only the weakest aversaries") || //Brink missions have a typo
                        (DifficultySelection.Easy == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will deal with only the weakest adversaries") ||
                        (DifficultySelection.Medium == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                        (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && !_settings["DoubleReward"].AsBool() && option.Value == "I will purge the temple of any and all assailants") ||
                        (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && _settings["DoubleReward"].AsBool() && !DoubleReward && option.Value == "I will challenge these invaders, as long as there aren't too many") ||
                        (DifficultySelection.Hard == (DifficultySelection)_settings["DifficultySelection"].AsInt32() && _settings["DoubleReward"].AsBool() && DoubleReward && option.Value == "I will purge the temple of any and all assailants")
                        )
                    {
                        NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                    }
                }
            }
            else if (dialogNpc.Name == "One Who Obeys Precepts")
            {
                foreach (KeyValuePair<int, string> option in options)
                {
                    if (option.Value == "Yes, I am ready.")
                    {
                        NpcDialog.SelectAnswer(dialogNpc.Identity, option.Key);
                    }
                }
            }
        }

        public static bool MissionExist()
        {
            if (Time.AONormalTime > missionTimer + 5)
            {
                if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                {
                    return false;
                }
            }

            return true;
        }

        private void OnTeamRequest(object sender, TeamRequestEventArgs e)
        {
            if (!_settings["Merge"].AsBool()) { return; }

            if (Leader != Identity.None)
            {
                if (e.Requester == Leader)
                {
                    e.Accept();
                }
            }
            else
            {
                Leader = e.Requester;
            }
        }

        private void BuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    if (!_settings["Enable"].AsBool())
                    {
                        _settings["Enable"] = true;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = true });
                        Start();
                    }
                    else
                    {
                        _settings["Enable"] = false;
                        IPCChannel.Broadcast(new StartStopIPCMessage() { IsStarting = false });
                        Stop();
                    }
                }
                Config.Save();
            }
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        public enum ModeSelection
        {
            Normal,
            Roam
        }
        public enum FactionSelection
        {
            Neutral,
            Clan,
            Omni
        }
        public enum DifficultySelection
        {
            Easy,
            Medium,
            Hard
        }

        public static int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;

            var lineMatch = Regex.Match(ex.StackTrace ?? "", @":line (\d+)$", RegexOptions.Multiline);

            if (lineMatch.Success)
            {
                lineNumber = int.Parse(lineMatch.Groups[1].Value);
            }
            return lineNumber;
        }
    }
}
