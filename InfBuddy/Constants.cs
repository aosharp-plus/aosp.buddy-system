﻿using AOSharp.Common.GameData;

namespace InfBuddy
{
    public static class Constants
    {
        public static Vector3 EntrancetoXantemple = new Vector3(2726.188, 25.83493, 3338.748);
        public static Vector3 TheRetainerOfErgo = new Vector3(2802.3, 25.5, 3375.8);

        public static Vector3 RedeemedGardenExit = new Vector3(388.5873, 114.5572, 416.2356);
        public static Vector3 UnredeemedGardenExit = new Vector3(461.8482, 39.90506, 449.5925);
        public static Vector3 PandeExitToInferno = new Vector3(116.8316, 49.53036, 25.0879);

        public static Vector3 OneWhoObeysPrecepts = new Vector3(186.3529, 1.01, 163.6912);
        public static Vector3 DefendPos = new Vector3(166.0f, 2.2f, 186.7f);
        public static Vector3 RoamPos = new Vector3(190.3f, 1.4f, 195.0f);
        public static Vector3 MissionExitToInferno = new Vector3(159.65, 2.698831, 101.4511);

        public static Vector3 LeechSpot = new Vector3(156.8f, 1.0f, 101.1f);
        public static Vector3 LeechMissionExit = new Vector3(161.2f, 2.7f, 104.2f);

        public const int Inferno = 4605;
        public const int OmniPandeGarden = 4697;
        public const int ClanPandeGarden = 4696;
        public const int Pande = 4328;
        public const int Mission = 9042;
    }
}
