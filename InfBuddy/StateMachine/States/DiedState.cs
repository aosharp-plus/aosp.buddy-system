﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System.Linq;
using AOSharp.Pathfinding;
using AOSharp.Core.UI;

namespace InfBuddy
{
    public class DiedState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Playfield.ModelIdentity.Instance == Constants.Inferno)
            {
                return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Died");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            var playerPosition = DynelManager.LocalPlayer.Position;
            var statue = DynelManager.AllDynels.Where(s => s.Name.Contains("Garden Exit")).FirstOrDefault();

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.ClanPandeGarden:
                case Constants.OmniPandeGarden:
                    if (Extensions.CanProceed())
                    {
                        if (playerPosition.DistanceFrom(statue.Position) > 5)
                        {
                            if (!SMovementController.IsNavigating())
                            {
                                SMovementController.SetNavDestination(statue.Position);
                            }
                        }
                        else if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            statue?.Use();
                        }
                    }
                    break;
                case Constants.Pande:
                    if (!SMovementController.IsNavigating())
                    {
                        SMovementController.SetNavDestination(Constants.PandeExitToInferno);
                    }
                    break;
            }
        }
    }
}
