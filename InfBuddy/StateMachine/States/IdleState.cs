﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;

namespace InfBuddy
{
    public class IdleState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (!InfBuddy.Enable) { return null; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Inferno:
                    if (!Team.IsInTeam)
                    {
                        return new ReformState();
                    }
                    else if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                    {
                        return new GrabMissionState();
                    }
                    else
                    {
                        return new EnterMissionState();
                    }

                case Constants.Mission:
                    if (InfBuddy.MissionExist())
                    {
                        if (InfBuddy._settings["Leech"].AsBool())
                        {
                            return new LeechState();
                        }
                        else if (DynelManager.NPCs.Any(c => c.Name == "One Who Obeys Precepts"))
                        {
                            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
                            {
                                return new StartMissionState();
                            }
                        }
                        else
                        {
                            if (InfBuddy._settings["ModeSelection"].AsInt32() == 0)
                            {
                                return new DefendSpiritState();
                            }
                            else
                            {
                                return new RoamState();
                            }
                        }

                    }
                    else if (DynelManager.NPCs.Any(c => c.FightingTarget?.Name != "Guardian Spirit of Purification"))
                    {
                        var corpse = DynelManager.Corpses.FirstOrDefault(c => c.Name.Contains("Remains of "));

                        if (InfBuddy._settings["Looting"].AsBool() && corpse != null)
                        {
                            return new LootingState();
                        }
                        else
                        {
                            return new ExitMissionState();
                        }
                    }

                    break;
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy.missionTimer = Time.AONormalTime;

            Chat.WriteLine("Idle");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
        }
    }
}
