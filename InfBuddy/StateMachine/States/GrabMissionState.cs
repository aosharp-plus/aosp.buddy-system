﻿using AOSharp.Core;
using System.Linq;
using AOSharp.Pathfinding;
using AOSharp.Core.UI;

namespace InfBuddy
{
    public class GrabMissionState : IState
    {
        private static bool _init = false;
        private double _timeToOpenDialog = 0;

        public IState GetNextState()
        {
            if (Game.IsZoning || !Team.IsInTeam) { return null; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Inferno:
                    if (Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                    {
                        if (Team.IsInTeam)
                        {
                            return new IdleState();
                        }
                    }
                    break;
            }


            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy._stateTimeOut = Time.NormalTime;

            _init = false;

            Chat.WriteLine("Grab mission");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            if (Game.IsZoning || !Team.IsInTeam) { return; }

            var TheRetainerOfErgo = DynelManager.NPCs.FirstOrDefault(c => c.Name == "The Retainer Of Ergo");
            var randoPos = Constants.TheRetainerOfErgo;
            randoPos.AddRandomness((int)1.34f);

            if (!SMovementController.IsNavigating())
            {
                if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.TheRetainerOfErgo) > 4f)
                {
                    SMovementController.SetNavDestination(randoPos);
                }
                else
                {
                    if (SMovementController.IsNavigating())
                    {
                        SMovementController.Halt();
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.TheRetainerOfErgo) < 8f)
                        {
                            if (TheRetainerOfErgo != null)
                            {
                                if (!_init)
                                {
                                    _init = true;
                                    _timeToOpenDialog = Time.NormalTime + 1;
                                }

                                if (_timeToOpenDialog > 0 && Time.NormalTime >= _timeToOpenDialog)
                                {
                                    NpcDialog.Open(TheRetainerOfErgo);
                                    _timeToOpenDialog = 0;
                                    _init = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}