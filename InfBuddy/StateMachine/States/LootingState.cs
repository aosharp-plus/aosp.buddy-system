﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Linq;
using AOSharp.Pathfinding;

namespace InfBuddy
{
    public class LootingState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Mission:

                    var mob = DynelManager.NPCs.Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                       && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 20
                       && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short))).FirstOrDefault();

                    var corpse = DynelManager.Corpses.FirstOrDefault(c => c.Name.Contains("Remains of "));

                    if (corpse == null || mob != null)
                    {
                        if (InfBuddy.ModeSelection.Normal == (InfBuddy.ModeSelection)InfBuddy._settings["ModeSelection"].AsInt32())
                        {
                            return new DefendSpiritState();
                        }
                        else
                        {
                            return new RoamState();
                        }
                    }
                    break;

                case Constants.Inferno:
                    return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Moving to corpse");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
        }

        public void Tick()
        {
            try
            {
                if (Game.IsZoning) { return; }

                var corpse = DynelManager.Corpses
                    .Where(c => c.Name.Contains("Remains of "))
                    .FirstOrDefault();

                if (Game.IsZoning || corpse == null) { return; }

                if (corpse != null)
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(corpse.Position) > 5f)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(corpse.Position);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred on line " + InfBuddy.GetLineNumber(ex) + ": " + ex.Message;

                if (errorMessage != InfBuddy.previousErrorMessage)
                {
                    Chat.WriteLine(errorMessage);
                    Chat.WriteLine("Stack Trace: " + ex.StackTrace);
                    InfBuddy.previousErrorMessage = errorMessage;
                }
            }
        }
    }
}