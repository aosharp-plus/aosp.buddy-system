﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class ReformState : IState
    {
        public static double reformDelay;
        private static double disbandDelay;
        double TimeOut;
        public static bool TeamCheck;

        public static List<Identity> TeamList = new List<Identity>();
        private List<Identity> InvitedPlayers = new List<Identity>();

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            if (Team.IsInTeam && !TeamCheck)
            {
                if (Time.AONormalTime > reformDelay)
                {
                    if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
                    {
                        if (TimeOut > Time.AONormalTime)
                        {
                            return new IdleState();
                        }
                        else if (TeamList.Count == Team.Members.Count)
                        {
                            return new IdleState();
                        }
                    }
                    else if (!Team.Members.Any(t => t.Character == null))
                    {
                        return new IdleState();
                    }
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            if (Game.IsZoning) { return; }

            TeamCheck = true;
            disbandDelay = 0;
            TimeOut = Time.AONormalTime + 60;

            Chat.WriteLine("Reforming");

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void OnStateExit()
        {
            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
            {
                InvitedPlayers.Clear();
                TeamList.Clear();
            }

            disbandDelay = 0;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (!Team.IsInTeam)
            {
                reformDelay = Time.AONormalTime + 6;

                TeamCheck = false;
            }

            if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
            {
                if (Team.IsInTeam && TeamCheck)
                {
                    if (!Team.Members.Any(t => t.Character == null))
                    {
                        if (disbandDelay == 0)
                        {
                            disbandDelay = Time.AONormalTime + 10;
                        }
                        else
                        {
                            if (Time.AONormalTime > disbandDelay)
                            {
                                Team.Disband();
                            }
                        }
                    }
                }
                if (!Team.IsInTeam || TeamList.Count == Team.Members.Count)
                {
                    InvitePlayers();
                }
            }
        }

        private void InvitePlayers()
        {
            foreach (var player in DynelManager.Players.Where(c => TeamList.Contains(c.Identity) //c.IsValid && c.IsInPlay && 
            && !Team.Members.Contains(c.Identity) && !InvitedPlayers.Contains(c.Identity) && c.Identity != DynelManager.LocalPlayer.Identity))
            {
                InvitedPlayers.Add(player.Identity);
                Team.Invite(player.Identity);
            }
        }
    }
}


