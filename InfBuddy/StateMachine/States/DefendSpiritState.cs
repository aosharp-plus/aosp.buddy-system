using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System.Linq;
using AOSharp.Pathfinding;

namespace InfBuddy
{
    public class DefendSpiritState : PositionHolder, IState
    {
        public DefendSpiritState() : base(Constants.DefendPos, 3f, 1)
        { }

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            var _corpse = DynelManager.Corpses
                .Where(c => c.Name.Contains("Remains of "))
                .FirstOrDefault();

            var mob = DynelManager.NPCs
                  .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                  && c.Position.DistanceFrom(DynelManager.LocalPlayer.Position) < 20
                  && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short))).FirstOrDefault();

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Mission:
                    if (InfBuddy._settings["Looting"].AsBool() && _corpse != null
                   && mob == null)
                    {
                        return new LootingState();
                    }

                    if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                    {
                        return new ExitMissionState();
                    }

                    if (InfBuddy._settings["ModeSelection"].AsInt32() == 1)
                    {
                        return new RoamState();
                    }

                    if (Time.AONormalTime > InfBuddy.missionTimeOut + 1200)
                    {
                        foreach (Mission mission in Mission.List)
                        {
                            if (mission.DisplayName.Contains("The Purification Ritual"))
                            {
                                Chat.WriteLine("Mission timed out, deleting.");
                                mission.Delete();
                            }
                        }
                    }
                    break;
                case Constants.Inferno:
                    return new IdleState();

            }
            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Defending");

            var randoPos = Constants.DefendPos;
            randoPos.AddRandomness((int)1.34f);

            if (!SMovementController.IsNavigating())
            {
                SMovementController.SetNavDestination(randoPos);
            }

            InfBuddy.missionTimeOut = Time.AONormalTime;
        }

        public void Tick()
        {
            if (Game.IsZoning) { return; }

            if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
            {
                var leader = Team.Members
                        .Where(c => c.Character?.Health > 0
                            && c.Character?.IsValid == true
                            && c.Identity == InfBuddy.Leader)
                        .FirstOrDefault()?.Character;

                if (leader != null)
                {
                    if (leader?.FightingTarget != null || leader?.IsAttacking == true)
                    {
                        var targetMob = DynelManager.NPCs
                            .Where(c => c.Health > 0
                                && c.Identity == leader?.FightingTarget?.Identity)
                            .FirstOrDefault();

                        if (targetMob != null)
                        {
                            if (targetMob.IsInLineOfSight && targetMob.IsInAttackRange())
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                                else
                                {
                                    if (DynelManager.LocalPlayer.FightingTarget == null
                                         && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                                    {
                                        InfBuddy.missionTimeOut = Time.AONormalTime;

                                        DynelManager.LocalPlayer.Attack(targetMob, false);
                                    }
                                }
                            }
                            else
                            {
                                if (!SMovementController.IsNavigating())
                                {
                                    SMovementController.SetNavDestination(targetMob.Position);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) > 5)
                        {
                            SMovementController.SetNavDestination(Constants.DefendPos, true);
                        }
                        else
                        {
                            HoldPosition();
                        }
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) > 5)
                    {
                        SMovementController.SetNavDestination(Constants.DefendPos, true);
                    }
                    else
                    {
                        HoldPosition();
                    }
                }
            }
            else
            {
                var mob = DynelManager.NPCs
                   .Where(c => c.Health > 0 && !c.Name.Contains("Guardian Spirit of Purification")
                   && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                   .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                   .ThenBy(c => c.HealthPercent)
                   .FirstOrDefault();

                if (mob != null)
                {
                    var distanceToTarget = mob.Position.DistanceFrom(Constants.DefendPos);

                    if (mob.IsInAttackRange() && mob.IsInLineOfSight)
                    {
                        if (SMovementController.IsNavigating())
                        {
                            SMovementController.Halt();
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                               && !DynelManager.LocalPlayer.IsAttacking
                               && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                InfBuddy.missionTimeOut = Time.AONormalTime;
                                DynelManager.LocalPlayer.Attack(mob, false);
                            }
                        }
                    }
                    else
                    {
                        if (distanceToTarget <= 60)
                        {
                            if (mob.IsInLineOfSight)
                            {
                                if (distanceToTarget > 20)
                                {
                                    Shared.TauntingTools.HandleTaunting(mob);
                                }
                                else
                                {
                                    SMovementController.SetNavDestination(mob.Position);
                                }
                            }
                            else
                            {
                                if (distanceToTarget < 20)
                                {
                                    SMovementController.SetNavDestination(mob.Position);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) > 5)
                    {
                        SMovementController.SetNavDestination(Constants.DefendPos, true);
                    }
                    else
                    {
                        HoldPosition();
                    }
                }
            }
        }

        public void OnStateExit()
        {
        }
    }

    public class PositionHolder
    {
        private readonly Vector3 _holdPos;
        private readonly float _HoldDist;
        private readonly int _entropy;

        public PositionHolder(Vector3 holdPos, float holdDist, int entropy)
        {
            _holdPos = holdPos;
            _HoldDist = holdDist;
            _entropy = entropy;
        }

        public void HoldPosition()
        {
            if (!SMovementController.IsNavigating() && !IsNearDefenseSpot())
            {
                var randomHoldPos = _holdPos;
                randomHoldPos.AddRandomness(_entropy);

                SMovementController.SetNavDestination(randomHoldPos);
            }
        }

        private bool IsNearDefenseSpot()
        {
            return DynelManager.LocalPlayer.Position.DistanceFrom(Constants.DefendPos) < _HoldDist;
        }
    }
}
