﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class ClearMissionState : IState
    {
        public static List<SimpleChar> mobs = new List<SimpleChar>();

        public static bool clear;
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Mission:
                    if (DynelManager.LocalPlayer.Identity == InfBuddy.Leader)
                    {
                        if (clear)
                        {
                            return new StartMissionState();
                        }
                    }
                    else
                    {
                        var Guardian = DynelManager.NPCs.FirstOrDefault(c => c.Health > 0 && c.Name.Contains("Guardian Spirit of Purification"));

                        if (Guardian != null)
                        {
                            if (InfBuddy._settings["ModeSelection"].AsInt32() == 0)
                            {
                                return new DefendSpiritState();
                            }
                            else
                            {
                                return new RoamState();
                            }
                        }
                    }
                    break;

                case Constants.Inferno:
                    return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Clearing mission");
            clear = false;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            if (Game.IsZoning || !Team.IsInTeam) { return; }

            mobs = DynelManager.NPCs
                   .Where(c => c.Health > 0 && !(c.Name.Contains("One Who Obeys Precepts")
                   || c.Name.Contains("Guardian Spirit of Purification"))
                   && !(c.Buffs.Contains(NanoLine.CharmOther) || c.Buffs.Contains(NanoLine.Charm_Short)))
                   .ToList();

            if (Playfield.ModelIdentity.Instance == Constants.Mission)
            {
                if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
                {
                    var leader = Team.Members
                       .Where(c => c.Character?.Health > 0
                           && c.Character?.IsValid == true
                           && c.Identity == InfBuddy.Leader)
                       .FirstOrDefault()?.Character;

                    if (leader != null)
                    {
                        if (leader?.FightingTarget != null || leader?.IsAttacking == true)
                        {
                            var targetMob = DynelManager.NPCs
                                .Where(c => c.Health > 0
                                    && c.Identity == (Identity)leader?.FightingTarget?.Identity)
                                .FirstOrDefault();

                            if (targetMob != null)
                            {
                                if (targetMob.IsInLineOfSight && targetMob.IsInAttackRange(true))
                                {
                                    if (SMovementController.IsNavigating())
                                    {
                                        SMovementController.Halt();
                                    }
                                    else
                                    {
                                        if (DynelManager.LocalPlayer.FightingTarget == null
                                             && !DynelManager.LocalPlayer.IsAttacking && !DynelManager.LocalPlayer.IsAttackPending)
                                        {
                                            DynelManager.LocalPlayer.Attack(targetMob, false);
                                        }
                                    }
                                }
                                if (!targetMob.IsInLineOfSight || !targetMob.IsInAttackRange())
                                {
                                    if (!SMovementController.IsNavigating())
                                    {
                                        SMovementController.SetNavDestination(targetMob.Position);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (DynelManager.LocalPlayer.Position.DistanceFrom((Vector3)leader?.Position) > 2f)
                            {
                                SMovementController.SetNavDestination((Vector3)leader?.Position);
                            }
                            else
                            {
                                if (SMovementController.IsNavigating())
                                {
                                    SMovementController.Halt();
                                }
                            }
                        }
                    }
                    else { return; }
                }
                else
                {
                    var mob = mobs.Where(c => c.Health > 0 && !(c.Name.Contains("One Who Obeys Precepts") || c.Name.Contains("Guardian Spirit of Purification")))
                     .OrderBy(c => c.Position.DistanceFrom(DynelManager.LocalPlayer.Position))
                     .ThenBy(c => c.HealthPercent)
                     .FirstOrDefault();

                    if (mob != null)
                    {
                        if (mob.IsInAttackRange(true) && mob.IsInLineOfSight)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget == null
                               && !DynelManager.LocalPlayer.IsAttacking
                               && !DynelManager.LocalPlayer.IsAttackPending)
                            {
                                DynelManager.LocalPlayer.Attack(mob, false);
                            }
                        }
                        else
                        {
                            SMovementController.SetNavDestination(mob.Position);
                        }
                    }
                    else
                    {
                        if (mobs.Count == 0)
                        {
                            clear = true;
                        }
                    }
                }
            }
        }

        public void OnStateExit()
        {
            clear = false;
        }
    }
}
