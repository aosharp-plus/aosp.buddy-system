﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System.Linq;

namespace InfBuddy
{
    public class LeechState : IState
    {
        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Mission:
                    if (!InfBuddy.MissionExist())
                    {
                        return new ExitMissionState();
                    }

                    if (!InfBuddy._settings["Leech"].AsBool())
                    {
                        return new IdleState();
                    }
                    break;

                case Constants.Inferno:
                    return new IdleState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            InfBuddy.missionTimer = Time.AONormalTime;

            Chat.WriteLine("Leecher");

            DynelManager.LocalPlayer.Position = Constants.LeechSpot;
            MovementController.Instance.SetMovement(MovementAction.Update);
            MovementController.Instance.SetMovement(MovementAction.JumpStart);
            MovementController.Instance.SetMovement(MovementAction.Update);
        }

        public void OnStateExit()
        {
            DynelManager.LocalPlayer.Position = new Vector3(160.4f, 2.6f, 103.0f);
        }

        public void Tick()
        {
        }
    }
}
