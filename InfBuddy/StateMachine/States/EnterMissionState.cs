﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Collections.Generic;
using System.Linq;

namespace InfBuddy
{
    public class EnterMissionState : IState
    {
        public static List<SimpleChar> mobs = new List<SimpleChar>();
        public static bool clear;
        private bool InTeam;
        double waitforlead;
        double teamDelay;

        public IState GetNextState()
        {
            if (Game.IsZoning) { return null; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.OmniPandeGarden:
                case Constants.ClanPandeGarden:
                    return new DiedState();
                case Constants.Inferno:
                    if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ri")))
                    {
                        return new GrabMissionState();
                    }
                    break;
                case Constants.Mission:
                    if (InfBuddy._settings["Leech"].AsBool())
                    {
                        return new LeechState();
                    }
                    else
                    {
                        if (DynelManager.LocalPlayer.Identity != InfBuddy.Leader)
                        {
                            var leader = DynelManager.Players.Where(l => l.Identity == InfBuddy.Leader).FirstOrDefault();

                            if (leader == null)
                            {
                                if (waitforlead == 0.0)
                                {
                                    waitforlead = Time.AONormalTime + 60;
                                }
                                else
                                {
                                    var Guardian = DynelManager.NPCs.FirstOrDefault(c => c.Health > 0 && c.Name.Contains("Guardian Spirit of Purification"));

                                    if (Time.AONormalTime > waitforlead || Guardian != null)
                                    {
                                        if (InfBuddy._settings["ModeSelection"].AsInt32() == 0)
                                        {
                                            return new DefendSpiritState();
                                        }
                                        else
                                        {
                                            return new RoamState();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (InTeam)
                                {
                                    return new ClearMissionState();
                                }
                            }
                        }
                        else
                        {
                            if (InTeam)
                            {
                                if (Time.AONormalTime > teamDelay)
                                {
                                    return new ClearMissionState();
                                }
                            }
                        }
                    }
                    break;
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("Entering");
            InTeam = false;
            waitforlead = 0.0;
            teamDelay = 0.0;

            if (SMovementController.IsNavigating())
            {
                SMovementController.Halt();
            }
        }

        public void Tick()
        {
            if (Game.IsZoning || !Team.IsInTeam) { return; }

            switch (Playfield.ModelIdentity.Instance)
            {
                case Constants.Inferno:
                    if (DynelManager.LocalPlayer.Position.DistanceFrom(Constants.EntrancetoXantemple) > 1)
                    {
                        if (!SMovementController.IsNavigating())
                        {
                            SMovementController.SetNavDestination(Constants.EntrancetoXantemple);
                        }
                    }
                    else
                    {
                        MovementController.Instance.SetMovement(MovementAction.ForwardStart);
                    }
                    break;
                case Constants.Mission:
                    if (MovementController.Instance.IsNavigating)
                    {
                        MovementController.Instance.Halt();
                    }
                    else
                    {
                        if (!Team.Members.Any(t => t.Character == null))
                        {
                            if (!InTeam)
                            {
                                teamDelay = Time.AONormalTime + 10;
                                InTeam = true;
                            }
                        }
                    }
                    break;
            }
        }

        public void OnStateExit()
        {
            waitforlead = 0.0;
            teamDelay = 0.0;
        }
    }
}
