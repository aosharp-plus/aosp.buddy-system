﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.IO;
using Path = System.IO.Path;

namespace Roamba
{
    public class RoamPathInitView : RoamPathView
    {
        private ComboBox _pathName;

        public RoamPathInitView(string path, RoamPathWindow window) : base(path, window)
        {
            try
            {
                if (Root.FindChild("AllPaths", out _pathName))
                {
                    if (!string.IsNullOrEmpty(Roamba.Config.RoamPath))
                        _pathName.SetText(Path.GetFileNameWithoutExtension(Roamba.Config.RoamPath));

                    var allFiles = Directory.GetFiles(Roamba.Config.RoamPathFolder);

                    for (int i = 0; i < allFiles.Length; i++)
                        _pathName.AppendItem(i + 1, Path.GetFileNameWithoutExtension(allFiles[i]));
                }

                if (Root.FindChild("CreateNewPath", out Button createPath))
                    createPath.Clicked = CreateNewPathClicked;

                if (Root.FindChild("EditCurrentPath", out Button editPath))
                    editPath.Clicked = EditCurrentPathClicked;

                if (Root.FindChild("LoadPath", out Button loadPath))
                    loadPath.Clicked = LoadClicked;
            }
            catch (Exception ex)
            {
                Roamba.Log.Warning(ex.Message);
            }
        }

        private void EditCurrentPathClicked(object sender, ButtonBase e)
        {
            if (Roamba.RoamPath.SPath == null)
            {
                Roamba.Log.Information("No path to edit");
                return;
            }

            if (Roamba.RoamPath.SPath.PlayfieldId != Playfield.ModelIdentity.Instance)
            {
                Roamba.Log.Information($"Cannot edit path saved for a different playfield.");
                return;
            }

            Load();
        }

        private void LoadClicked(object sender, ButtonBase e)
        {
            var fullPath = $"{CommonParameters.PluginDataPath}\\RoamPath\\" + _pathName.GetText() + ".json";

            if (!File.Exists(fullPath))
            {
                Roamba.Log.Information($"File not found '{fullPath}'");
                return;
            }

            if (Roamba.RoamPath.SPath != null)
                Roamba.RoamPath.SPath.Delete();

            var roamPath = RoamPath.Load(fullPath);

            Roamba.RoamPath.SPath = roamPath.SPath;
            Roamba.RoamPath.Rules = roamPath.Rules;

            if (Roamba.RoamPath.SPath.PlayfieldId != Playfield.ModelIdentity.Instance)
            {
                Roamba.Log.Information($"Cannot load a path saved for a different playfield.");
                Roamba.RoamPath.SPath.Delete();
                Roamba.RoamPath.SPath = null;
                return;
            }

            Roamba.Config.RoamPath = fullPath;
            Roamba.Config.Save();
            Load();
        }

        private void CreateNewPathClicked(object sender, ButtonBase e)
        {
            if (Roamba.RoamPath.SPath != null)
                Roamba.RoamPath.SPath.Delete();

            Roamba.RoamPath.SPath = SPath.Create();
            Roamba.RoamPath.Rules = new TargetingRules();

            Load();
        }

        private void Load()
        {
            Roamba.RoamPath.SPath.IsLocked = Roamba.RoamPath.SPath.Waypoints.Count != 0;
            Dispose();
            Parent.LoadMainView();
        }
    }
}