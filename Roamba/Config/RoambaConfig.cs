﻿using AOSharp.Common.GameData;
using Newtonsoft.Json;
using Shared;

namespace Roamba
{
    public class RoambaConfig : BuddyBaseConfig<RoambaConfig>
    {
        [JsonIgnore]
        public override string FileName => "RoambaConfig";

        [JsonIgnore]
        public override RoambaConfig LoadDefaults => new RoambaConfig();

        [JsonIgnore]
        public string RoamPathFolder;

        public BuddyCoreConfig CoreConfig;

        public PathConfig PathingConfig;

        public string RoamPath;

        public Vector2 WindowCoords;

        public RoambaConfig()
        {
            CoreConfig = new BuddyCoreConfig();
            PathingConfig = new PathConfig();
            RoamPath = "";
            WindowCoords = Vector2.Zero;
        }
    }
}
