﻿using System.Collections.Generic;

namespace Roamba
{
    public class TargetingRules
    {
        public List<string> IgnoredNames;
        public List<string> PriorityNames;

        public TargetingRules()
        {
            IgnoredNames = new List<string>();
            PriorityNames = new List<string>();
        }
    }
}
