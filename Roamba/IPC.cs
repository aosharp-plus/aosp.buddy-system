﻿using AOSharp.Core;
using AOSharp.Core.IPC;
using Roamba.IPCMessages;

namespace Roamba
{
    public class IPC : IPCChannelBase
    {
        protected override int _localDynelId => DynelManager.LocalPlayer.Identity.Instance;
        internal byte ChannelId;

        public IPC(byte channelId) : base(channelId)
        {
            ChannelId = channelId;
            RegisterCallback((int)IPCOpcode.Enabled, OnEnabledMessage);
        }

        public new void SetChannelId(byte channelId)
        {
            ChannelId = channelId;
            base.SetChannelId(channelId);
        }

        private void OnEnabledMessage(int arg1, IPCMessage message)
        {
            EnabledIPCMessage enabledIpc = (EnabledIPCMessage)message;

            if (!string.IsNullOrEmpty(enabledIpc.RoamPath) && Roamba.RoamPath.SPath != null)
            {
                Roamba.RoamPath.SPath.Delete();
                Roamba.RoamPath = RoamPath.Load(enabledIpc.RoamPath);
                Roamba.Config.RoamPath = enabledIpc.RoamPath;
                Roamba.Config.Save();
            }

            if (Roamba.RoamPath.SPath.PlayfieldId != Playfield.ModelIdentity.Instance)
                return;

            Roamba.MainWindow.OnEnabledPress(enabledIpc.SetEnabled);
        }
    }
}
